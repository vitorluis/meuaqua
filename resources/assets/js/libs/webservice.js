/**
 * Created by vitor on 08/11/16.
 */
function Webservice() {
}

Webservice.prototype.globalTimeOut = 120000;  //2 minutes

Webservice.prototype.post = function (pURL, pData, pSuccessCallback, pFailCallback, pAsync) {
    var asynchronus = true;

    if (pAsync != undefined && pAsync == false) {
        asynchronus = false;
    }

    // Call the AJAX
    $.ajax({
        type: "POST",
        contentType: false,
        processData: false,
        async: asynchronus,
        timeout: this.globalTimeOut,
        url: pURL,
        data: pData,
        error: function (request, status, error) {
            if (pFailCallback != undefined && typeof pFailCallback == "function") {
                pFailCallback(request, status, error);
            }
        },
        success: function (data) {
            if (pSuccessCallback != undefined && typeof pSuccessCallback == "function") {
                pSuccessCallback(data);
            }
        }
    });
};

Webservice.prototype.put = function (pURL, pData, pSuccessCallback, pFailCallback, pAsync) {
    var asynchronus = true;
    if (pAsync != undefined && pAsync == false) {
        asynchronus = false;
    }

    // Call the AJAX
    $.ajax({
        type: "PUT",
        dataType: "json",
        async: asynchronus,
        timeout: this.globalTimeOut,
        cache: false,
        url: pURL,
        data: JSON.stringify(pData),
        error: function (request, status, error) {
            if (pFailCallback != undefined && typeof pFailCallback == "function") {
                pFailCallback(request, status, error);
            }
        },
        success: function (data) {
            if (pSuccessCallback != undefined && typeof pSuccessCallback == "function") {
                pSuccessCallback(data);
            }
        }
    });
};

Webservice.prototype.delete = function (pURL, pSuccessCallback, pFailCallback, pAsync) {
    var asynchronus = true;
    if (pAsync != undefined && pAsync == false) {
        asynchronus = false;
    }

    // Call the AJAX
    $.ajax({
        type: "DELETE",
        dataType: "json",
        async: asynchronus,
        timeout: this.globalTimeOut,
        cache: false,
        url: pURL,
        error: function (request, status, error) {
            if (pFailCallback != undefined && typeof pFailCallback == "function") {
                pFailCallback(request, status, error);
            }
        },
        success: function (data) {
            if (pSuccessCallback != undefined && typeof pSuccessCallback == "function") {
                pSuccessCallback(data);
            }
        }
    });
};

Webservice.prototype.get = function (pURL, pSuccessCallback, pFailCallback, pAsync) {
    var asynchronus = true;
    if (pAsync != undefined && pAsync == false) {
        asynchronus = false;
    }

    // Call the AJAX
    $.ajax({
        type: "GET",
        dataType: "json",
        async: asynchronus,
        timeout: this.globalTimeOut,
        cache: false,
        url: pURL,
        error: function (request, status, error) {
            if (pFailCallback != undefined && typeof pFailCallback == "function") {
                pFailCallback(request, status, error);
            }
        },
        success: function (data) {
            if (pSuccessCallback != undefined && typeof pSuccessCallback == "function") {
                pSuccessCallback(data);
            }
        }
    });
};
