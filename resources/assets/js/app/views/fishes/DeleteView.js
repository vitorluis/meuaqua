/**
 * Created by vitor on 06/01/17.
 */
var FishesDeleteView = Backbone.View.extend({
    el: $('body'),
    fishId: null,
    events: {
        "click .btn-delete-fish": "openFishModal",
        "click .btn-delete-fishes": "deleteFishes"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openFishModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.fishId = $btn.data('fish');

        // Open the modal
        $('.delete-fish-modal').modal('show');
    },
    deleteFishes: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/fishes/delete/' + this.fishId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();
            return;
        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
