/**
 * Created by vitor on 06/01/17.
 */
var FishesAddView = Backbone.View.extend({
    el: $('body'),
    fishType: null,
    quantity: null,
    aquarium: null,
    events: {
        "click .btn-add-fish": "openFishModal",
        "click .btn-save-fishes": "saveFishes",
        "submit #new-fish": "saveFishes",
        "change .fish-type": "changeSelectedFish"
    },
    initialize: function () {
        this.changeSelectedFish();

        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openFishModal: function () {
        $('.new-fish-modal').modal('show');
    },
    saveFishes: function (evt) {
        evt.preventDefault();

        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#new-fish')[0]);
        data.append('aquarium', this.aquarium);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/fishes/save', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg').html(errors_str);
        $('.alert-error').show();
    },
    changeSelectedFish: function () {
        var selected = $('.fish-type :selected').data('image');
        $('.selected-fish-type').attr('src', selected);
    }
});
