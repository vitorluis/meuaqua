/**
 * Created by vitor on 06/01/17.
 */
var FishesEditView = Backbone.View.extend({
    el: $('body'),
    fishId: null,
    quantity: null,
    events: {
        "click .btn-edit-fish": "openFishModal",
        "click .btn-save-fish-amount": "saveFishes",
        "submit #edit-fish": "saveFishes"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openFishModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.fishId = $btn.data('fish');
        this.quantity = $btn.data('amount');

        // Set the current amount on input
        $('.fish-edit-quantity').val(this.quantity);

        // Open the modal
        $('.edit-fish-modal').modal('show');
    },
    saveFishes: function (evt) {
        evt.preventDefault();

        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-fish')[0]);
        data.append('fish_id', this.fishId);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/fishes/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
