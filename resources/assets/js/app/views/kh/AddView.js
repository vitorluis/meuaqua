/**
 * Created by vitor on 06/01/17.
 */
var KhAddView = Backbone.View.extend({
    el: $('body'),
    khType: null,
    quantity: null,
    aquarium: null,
    events: {
        "click .btn-add-kh": "openKhModal",
        "click .btn-save-kh": "saveKh",
        "submit #new-kh": "saveKh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();

        // Initialize the data table
        $('.data-table').dataTable({
            "pageLength": 10,
            "order": [[1, "desc"]],
            "language": {
                "lengthMenu": "Mostrar _MENU_ linhas por página",
                "zeroRecords": "Nenhum Resultado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtered from _MAX_ total records)",
                paginate: {
                    previous: '‹',
                    next: '›'
                },
                aria: {
                    paginate: {
                        previous: 'Anterior',
                        next: 'Próximo'
                    }
                }
            }
        });
    },
    openKhModal: function () {
        $('.new-kh-modal').modal('show');
    },
    saveKh: function (evt) {
        evt.preventDefault();

        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#new-kh')[0]);
        data.append('aquarium', this.aquarium);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/kh/save', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg').html(errors_str);
        $('.alert-error').show();
    }
});
