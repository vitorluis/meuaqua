/**
 * Created by vitor on 06/01/17.
 */
var KhEditView = Backbone.View.extend({
    el: $('body'),
    khId: null,
    level: null,
    events: {
        "click .btn-edit-kh": "openKhModal",
        "click .btn-save-kh-level": "saveKh",
        "submit #edit-kh": "saveKh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openKhModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.khId = $btn.data('kh');
        this.level = $btn.data('level');

        // Set the current level on input
        $('.kh-edit-level').val(this.level);

        // Open the modal
        $('.edit-kh-modal').modal('show');
    },
    saveKh: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-kh')[0]);
        data.append('kh_id', this.khId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/kh/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();
        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
