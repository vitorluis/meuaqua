/**
 * Created by vitor on 06/01/17.
 */
var KhDeleteView = Backbone.View.extend({
    el: $('body'),
    khId: null,
    events: {
        "click .btn-delete-kh": "openKhModal",
        "click .btn-delete-kh-level": "deleteKh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openKhModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.khId = $btn.data('kh');

        // Open the modal
        $('.delete-kh-modal').modal('show');
    },
    deleteKh: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/kh/delete/' + this.khId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();
        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
