/**
 * Created by vitor on 06/01/17.
 */
var TemperatureDeleteView = Backbone.View.extend({
    el: $('body'),
    temperatureId: null,
    events: {
        "click .btn-delete-temperature": "openTemperatureModal",
        "click .btn-delete-temperature-degrees": "deleteTemperature"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openTemperatureModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.temperatureId = $btn.data('temperature');

        // Open the modal
        $('.delete-temperature-modal').modal('show');
    },
    deleteTemperature: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/temperature/delete/' + this.temperatureId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
