/**
 * Created by vitor on 06/01/17.
 */
var TemperatureEditView = Backbone.View.extend({
    el: $('body'),
    temperatureId: null,
    degrees: null,
    events: {
        "click .btn-edit-temperature": "openTemperatureModal",
        "click .btn-save-temperature-degrees": "saveTemperature",
        "submit #edit-temperature": "saveTemperature"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openTemperatureModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.temperatureId = $btn.data('temperature');
        this.degrees = $btn.data('degrees');

        // Set the current degrees on input
        $('.temperature-edit-degrees').val(this.degrees);

        // Open the modal
        $('.edit-temperature-modal').modal('show');
    },
    saveTemperature: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-temperature')[0]);
        data.append('temperature_id', this.temperatureId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/temperature/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
