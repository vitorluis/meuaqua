/**
 * Created by vitor on 06/01/17.
 */
var TPAEditView = Backbone.View.extend({
    el: $('body'),
    tpaId: null,
    date: null,
    perc: null,
    events: {
        "click .btn-edit-tpa": "openTPAModal",
        "click .btn-save-tpa-date": "saveTPA",
        "submit #edit-tpa": "saveTPA"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();

        $('.tpa-edit-date').datepicker({
            format: "dd/mm/yyyy",
            orientation: "top auto",
            autoclose: true
        });
    },
    openTPAModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.tpaId = $btn.data('tpa');
        this.date = $btn.data('date');
        this.perc = $btn.data('perc');

        // Set the current date on input
        $('.tpa-edit-date').val(this.date);
        $('.tpa-edit-perc').val(this.perc);

        // Open the modal
        $('.edit-tpa-modal').modal('show');
    },
    saveTPA: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-tpa')[0]);
        data.append('tpa_id', this.tpaId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/tpa/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();
        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
