/**
 * Created by vitor on 06/01/17.
 */
var TPADeleteView = Backbone.View.extend({
    el: $('body'),
    tpaId: null,
    events: {
        "click .btn-delete-tpa": "openTPAModal",
        "click .btn-delete-tpa-level": "deleteTPA"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openTPAModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.tpaId = $btn.data('tpa');

        // Open the modal
        $('.delete-tpa-modal').modal('show');
    },
    deleteTPA: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/tpa/delete/' + this.tpaId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
