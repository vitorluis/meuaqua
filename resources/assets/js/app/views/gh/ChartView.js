/**
 * Created by vitor on 12/02/17.
 */
var GhChartView = Backbone.View.extend({
    el: $('body'),
    canvas: null,
    data: null,
    chart: null,
    initialize: function () {
        // Get the canvas
        this.canvas = document.getElementById("chart").getContext("2d");

        // Create the data
        this.createData();

        // Draw the chart
        this.renderChart();
    },
    createData: function () {
        // Get the levels and measure dates
        var levels = [];
        var dates = [];

        $('.gh-table tbody tr').each(function (i, row) {
            if ($(row).attr('data-level') !== undefined) {
                // set the level
                levels.push($(row).data('level'));

                // Set the date
                var date = $(row).data('measure').split(' ');
                dates.push(date[0]);
            }
        });

        // Create the data
        this.data = {
            // yLabels: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14'],
            labels: dates.reverse(),
            datasets: [{
                label: "GH",
                fillColor: "rgba(34,186,160,0.2)",
                strokeColor: "rgba(34,186,160,1)",
                pointColor: "rgba(34,186,160,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(18,175,203,1)",
                backgroundColor: "rgba(187, 180, 37, 0.4)",
                data: levels.reverse()
            }]
        };
    },
    renderChart: function () {
        this.chart = new Chart(this.canvas, {
            type: 'line',
            data: this.data,
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            responsive: true,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 10,
                            min: 0,
                            stepSize: 1
                        }
                    }]
                }
            }
        });
    }
});