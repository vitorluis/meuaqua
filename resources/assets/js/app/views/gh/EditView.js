/**
 * Created by vitor on 06/01/17.
 */
var GhEditView = Backbone.View.extend({
    el: $('body'),
    ghId: null,
    level: null,
    events: {
        "click .btn-edit-gh": "openGhModal",
        "click .btn-save-gh-level": "saveGh",
        "submit #edit-gh": "saveGh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openGhModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.ghId = $btn.data('gh');
        this.level = $btn.data('level');

        // Set the current level on input
        $('.gh-edit-level').val(this.level);

        // Open the modal
        $('.edit-gh-modal').modal('show');
    },
    saveGh: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-gh')[0]);
        data.append('gh_id', this.ghId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/gh/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();
        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
