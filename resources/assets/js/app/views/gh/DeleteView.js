/**
 * Created by vitor on 06/01/17.
 */
var GhDeleteView = Backbone.View.extend({
    el: $('body'),
    ghId: null,
    events: {
        "click .btn-delete-gh": "openGhModal",
        "click .btn-delete-gh-level": "deleteGh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openGhModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.ghId = $btn.data('gh');

        // Open the modal
        $('.delete-gh-modal').modal('show');
    },
    deleteGh: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/gh/delete/' + this.ghId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
