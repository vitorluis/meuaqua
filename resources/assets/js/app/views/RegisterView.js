var RegisterView = Backbone.View.extend({
    el: $('.form-register'),
    initialize: function () {
        // Set the values of user latitude and longitude
        navigator.geolocation.getCurrentPosition(this.setUserLocation);

        // Get workers fields
        this.workersData = $('.workers-data');

        // Call the function showworkerdata to show or not the workers fields
        this.showWorkersData();
    },
    events: {
        "change .user-type": "showWorkersData"
    },
    showWorkersData: function (evt) {
        // Get the selected radio
        this.radioUserType = $('.user-type:checked');

        // Check the value of the radio box
        this.radioUserType.val() == 'user' ? this.workersData.addClass('hidden') : this.workersData.removeClass('hidden');
    },
    setUserLocation: function (location) {
        // Set the location on correct fields
        $('.user-latitude').val(location.coords.latitude);
        $('.user-longitude').val(location.coords.longitude);
    }
});
