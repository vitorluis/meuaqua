/**
 * Created by vitor on 07/11/16.
 */
var search_scope = null;
var SearchView = Backbone.View.extend({
    el: $('body'),
    initialize: function () {
        // Set the search_scope
        search_scope = this;

        // Get the search input
        this.searchInput = $('.search-input');

        // Check on jStorage if have to do some search
        if ($.jStorage.get("search")) {
            // Set the text search on form
            this.searchInput.val($.jStorage.get('search_text'));

            // Show the search bar
            $('.search-form').css('margin-top', '0px');

            // Perform the search
            this.performSearch();
        }

        // Check ESC key
        $(document).keyup(function (e) {
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                // Close the search
                $('.search-form').css('margin-top', '-60px');

                // And close the sidebars
                $('body').find('.user-sidebar').trigger('sidebar:close');
            }
        });
    },
    events: {
        "click .show-search": "showSearchForm",
        "click .close-search": "hideSearchForm",
        "submit .search-form": "searchWorkers",
        "click .close-sidebar": "closeSidebar"
    },
    showSearchForm: function () {
        $('.search-form').css('margin-top', '0');
        $('.search-input').focus();
    },
    hideSearchForm: function () {
        $('.search-form').css('margin-top', '-60px');
    },
    searchWorkers: function (e) {
        // Prevent to send the form
        e.preventDefault();

        // Set on jStorage, that has a search to do
        $.jStorage.set('search', true);
        $.jStorage.set('search_text', this.searchInput.val());

        // Close all sidebars
        $(".sidebar-user-info").trigger("sidebar:close");
        $(".sidebar-user-info-bottom").trigger("sidebar:close");

        // Call the function, who will call an AJAX
        this.performSearch();
    },

    performSearch: function () {
        // First of all, check the URL
        if (window.location.pathname != '/') {
            // Redirect user to home
            window.location.href = '/';
            return;
        }

        // Remove the jStorage flag
        $.jStorage.deleteKey('search');

        // Get the content to search
        var search = $.jStorage.get('search_text');
        $.jStorage.deleteKey('search_text');

        // Make the call
        var webService = new Webservice();
        webService.post('/api/search', {'text': search}, this.searchResults, this.failSearch);
    },
    searchResults: function (workers) {
        // Clear other toasts, if have one
        toastr.clear();

        // Get the map
        search_scope.map = window.mapView.getMap();

        // Remove all markers
        search_scope.map.removeMarkers();

        // Set new zoom
        search_scope.map.changeZoom(12);

        // Check the number of documents found
        if (workers.numFound > 0) {
            for (var i in workers.data) {
                // Split the latitude and longitude of the user
                var coords = workers.data[i].user_location.split(',');

                // Create a new Marker
                var marker = new Marker();
                marker.setLatitude(coords[0]);
                marker.setLongitude(coords[1]);
                marker.setMap(search_scope.map);
                marker.setTitle(workers.data[i].name);
                marker.setIcon('assets/images/user-map.png');

                // Set user data
                marker.setData(workers.data[i]);

                // Set a listener, when click, load user data
                marker.clickListener(function () {
                    search_scope.clickMarker(marker);
                });

                // Init the marker
                marker.init();

                // Add Marker on map
                search_scope.map.addMarker(marker);

                // If is the first marker, set this on center of map
                if (i == 0) {
                    search_scope.map.changeCenterPosition(coords[0], coords[1]);
                }
            }
        }
        else {
            // Show a toast telling user any worker match
            toastr.warning('Nenhum usuário encontrado, tente novamente!', 'Atenção');
        }
    },
    failSearch: function () {
        // Show a toast with the error
        toastr.error('Não foi possível fazer a pesquisa, tente novamente', 'Atenção');
    },
    closeSidebar: function (evt) {
        // Find the right sidebar and close it.
        $(evt.target).parents('.user-sidebar').trigger('sidebar:toggle');
    },
    clickMarker: function (marker) {
        // Get data
        var data = marker.getData();

        // Show the sidebar
        if (!isMobile.phone && !isMobile.tablet) {
            // Get and render the template
            var template_right_sidebar = _.template($('#user-sidebar-right-data').html());

            // Get the sidebar
            var $right_sidebar = $(".sidebar-user-info");
            $right_sidebar.html(template_right_sidebar(data));
            $right_sidebar.trigger("sidebar:open");
        }
        else {
            // Get and render the template
            var template_bottom_sidebar = _.template($('#user-sidebar-bottom-data').html());

            // Get the sidebar
            var $bottom_sidebar = $(".sidebar-user-info-bottom");
            $bottom_sidebar.html(template_bottom_sidebar(data));
            $bottom_sidebar.trigger("sidebar:open");
        }
    }
});