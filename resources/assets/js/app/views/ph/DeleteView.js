/**
 * Created by vitor on 06/01/17.
 */
var PhDeleteView = Backbone.View.extend({
    el: $('body'),
    phId: null,
    events: {
        "click .btn-delete-ph": "openPhModal",
        "click .btn-delete-ph-level": "deletePh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openPhModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.phId = $btn.data('ph');

        // Open the modal
        $('.delete-ph-modal').modal('show');
    },
    deletePh: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/ph/delete/' + this.phId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
