/**
 * Created by vitor on 06/01/17.
 */
var PhEditView = Backbone.View.extend({
    el: $('body'),
    phId: null,
    level: null,
    events: {
        "click .btn-edit-ph": "openPhModal",
        "click .btn-save-ph-level": "savePh",
        "submit #edit-ph": "savePh"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openPhModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.phId = $btn.data('ph');
        this.level = $btn.data('level');

        // Set the current level on input
        $('.ph-edit-level').val(this.level);

        // Open the modal
        $('.edit-ph-modal').modal('show');
    },
    savePh: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-ph')[0]);
        data.append('ph_id', this.phId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/ph/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
