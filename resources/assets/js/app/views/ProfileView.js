var ProfileView = Backbone.View.extend({
    el: $('body'),
    initialize: function () {
        // Initialize the select plugin
        $('select').select2({
            placeholder: 'Selecione os serviços que você presta',
            maximumSelectionLength: 4
        });

        // Set the values of user latitude and longitude
        navigator.geolocation.getCurrentPosition(this.setUserLocation);
    },
    events: {
        "change .avatar-upload": "changeProfilePhoto"
    },
    changeProfilePhoto: function (evt) {
        var raw_file = evt.target.files[0];

        // Check if is an image type
        var regex = /image\/*/i;
        if (regex.test(raw_file.type)) {
            // Get the image path
            var image_path = URL.createObjectURL(evt.target.files[0]);

            // Change the image
            $('.profile-avatar').attr('src', image_path);
        }
    },
    setUserLocation: function (location) {
        // Set the location on correct fields
        $('.user-latitude').val(location.coords.latitude);
        $('.user-longitude').val(location.coords.longitude);
    }
});