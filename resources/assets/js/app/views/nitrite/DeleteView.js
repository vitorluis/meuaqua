/**
 * Created by vitor on 06/01/17.
 */
var NitriteDeleteView = Backbone.View.extend({
    el: $('body'),
    nitriteId: null,
    events: {
        "click .btn-delete-nitrite": "openNitriteModal",
        "click .btn-delete-nitrite-level": "deleteNitrite"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openNitriteModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.nitriteId = $btn.data('nitrite');

        // Open the modal
        $('.delete-nitrite-modal').modal('show');
    },
    deleteNitrite: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/nitrite/delete/' + this.nitriteId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
