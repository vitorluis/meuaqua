/**
 * Created by vitor on 06/01/17.
 */
var NitriteEditView = Backbone.View.extend({
    el: $('body'),
    nitriteId: null,
    level: null,
    events: {
        "click .btn-edit-nitrite": "openNitriteModal",
        "click .btn-save-nitrite-level": "saveNitrite",
        "submit #edit-nitrite": "saveNitrite"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openNitriteModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.nitriteId = $btn.data('nitrite');
        this.level = $btn.data('level');

        // Set the current level on input
        $('.nitrite-edit-level').val(this.level);

        // Open the modal
        $('.edit-nitrite-modal').modal('show');
    },
    saveNitrite: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-nitrite')[0]);
        data.append('nitrite_id', this.nitriteId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/nitrite/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
