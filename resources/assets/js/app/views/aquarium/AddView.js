/**
 * Created by vitor on 06/01/17.
 */
var AquariumAddView = Backbone.View.extend({
    el: $('body'),
    width: null,
    length: null,
    height: null,
    liters: null,
    events: {
        "click .new-aquarium-btn": "openAquariumModal",
        "click .btn-save-aquarium": "saveAquarium",
        "change .aquarium-width": "calculateLiters",
        "change .aquarium-length": "calculateLiters",
        "change .aquarium-height": "calculateLiters"
    },
    openAquariumModal: function () {
        $('.new-aquarium-modal').modal('show');
    },
    saveAquarium: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the form Data
        var data = new FormData($('#new-aquarium')[0]);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('aquarium/new', data, this.successCallback, this.failCallback, true)
    },
    calculateLiters: function () {
        var width = parseInt($('.aquarium-width').val());
        var length = parseInt($('.aquarium-length').val());
        var height = parseInt($('.aquarium-height').val());

        // Check if all variables if filled up
        if (!isNaN(width) && !isNaN(length) && !isNaN(height)) {
            $('.aquarium-liters').val(Math.round((width * length * height) / 1000));
        }
        else {
            $('.aquarium-liters').val('');
        }
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();
            return;
        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg').html(errors_str);
        $('.alert-error').show();
    }
});