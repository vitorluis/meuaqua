/**
 * Created by vitor on 27/01/17.
 */
var AquariumEditView = Backbone.View.extend({
    el: $('body'),
    aquarium: null,
    width: null,
    length: null,
    height: null,
    liters: null,
    events: {
        "click .btn-edit-aquarium": "editAquarium",
        "click .btn-save-edit-aquarium": "saveAquarium",
        "change .aquarium-width": "calculateLiters",
        "change .aquarium-length": "calculateLiters",
        "change .aquarium-height": "calculateLiters"
    },
    editAquarium: function (evt) {
        // Get the aquarium Id
        this.aquarium = $(evt.currentTarget).data('aquarium');

        // Get the aquarium data
        var ws = new Webservice();
        ws.get('/aquarium/get/' + this.aquarium, this.openAquariumModal);

    },
    openAquariumModal: function (data) {
        // Set the data on the fields
        $('#edit-aquarium .aquarium-name').val(data.name);
        $('#edit-aquarium .aquarium-water-type').val(data.freshwater ? 'doce' : 'salgada');
        $('#edit-aquarium .aquarium-length').val(data.length);
        $('#edit-aquarium .aquarium-width').val(data.width);
        $('#edit-aquarium .aquarium-height').val(data.height);
        $('#edit-aquarium .aquarium-liters').val(data.liters);
        $('#edit-aquarium .aquarium-description').text(data.description);

        // Open the modal
        $('.edit-aquarium-modal').modal('show');
    },
    saveAquarium: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the form Data
        var data = new FormData($('#edit-aquarium')[0]);
        data.append('aquarium', this.aquarium);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/aquarium/edit', data, this.successCallback, this.failCallback, true)
    },
    calculateLiters: function () {
        var width = parseInt($('#edit-aquarium .aquarium-width').val());
        var length = parseInt($('#edit-aquarium .aquarium-length').val());
        var height = parseInt($('#edit-aquarium .aquarium-height').val());

        // Check if all variables if filled up
        if (!isNaN(width) && !isNaN(length) && !isNaN(height)) {
            $('#edit-aquarium .aquarium-liters').val(Math.round((width * length * height) / 1000));
        }
        else {
            $('#edit-aquarium .aquarium-liters').val('');
        }
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
