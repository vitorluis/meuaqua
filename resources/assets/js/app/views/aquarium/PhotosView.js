/**
 * Created by vitor on 06/01/17.
 */
var AquariumPhotosView = Backbone.View.extend({
    el: $('body'),
    aquarium: null,
    dropzone: null,
    gallery: null,
    photoId: null,
    events: {
        "click .btn-add-photos": "openAddPhotosModal",
        "click .btn-send-photos": "sendPhotos",
        "click .btn-cancel-photos": "removeAllFiles",
        "click .btn-delete-current-photo": "openDeletePhotoModal",
        "click .btn-delete-photo": "deletePhoto"
    },
    initialize: function () {
        // Get aquarium ID
        this.aquarium = $('.aquarium-id').val();

        // Disable auto discover
        Dropzone.autoDiscover = false;

        // Starts Dropzone
        var self = this;
        $('.photo-upload').dropzone({
            url: '/aquarium/photo/' + this.aquarium,
            parallelUploads: 20,
            paramName: "photo",
            autoProcessQueue: false,
            init: function () {
                self.dropzone = this;
            },
            error: self.failSendingFiles,
            queuecomplete: self.successSendingFiles
        });

        // Set dropzone message
        $('.dz-message.dz-default').html('<h1 style="text-align: center"><strong>Arraste uma foto para cá, ou clique para escolher.<strong></h1>');

        // Create gallery
        var $image = $('#photo-gallery img');
        if ($image.length > 0) {
            this.gallery = $('#photo-gallery').unitegallery({
                theme_enable_fullscreen_button: true,
                theme_enable_play_button: true,
                theme_enable_hidepanel_button: true,
                theme_enable_text_panel: true,
                gallery_theme: "compact"
            });
        }
        if (this.gallery != null) {
            this.gallery.on("item_change", this.updatePhotoId);
            this.gallery.getItem(0);
        }
    },
    openAddPhotosModal: function (evt) {
        evt.preventDefault();
        $('.add-photos').modal('show');
    },
    sendPhotos: function (evt) {
        var files = this.dropzone.files.length;
        if (files > 0) {
            this.dropzone.processQueue();
        } else {
            $('.alert-error > .error-message').html('Você deve selecionar pelo menos uma foto.');
            $('.alert-error').show();
        }
    },
    deletePhoto: function (evt) {
        evt.preventDefault();
        this.photoId = $('.btn-delete-current-photo').data('photo-id');

        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/aquarium/photo/delete/' + this.photoId, this.successDelete, this.failDelete);

    },
    updatePhotoId: function (i, data) {
        $('.btn-delete-current-photo').attr("data-photo-id", data.photoId);
    },
    successSendingFiles: function () {
        window.location.reload();
    },
    failSendingFiles: function () {
        $('.alert-error > .error-message').html('Falha ao enviar as fotos, tente novamente.');
        $('.alert-error').show();
    },
    removeAllFiles: function () {
        this.dropzone.removeAllFiles();
    },
    openDeletePhotoModal: function (evt) {
        evt.preventDefault();
        $('.delete-photo-modal').modal('show');
    },
    successDelete: function (data) {
        if (data.response) {
            window.location.reload();
        }
    },
    failDelete: function (error) {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});