/**
 * Created by vitor on 06/01/17.
 */
var AmmoniaDeleteView = Backbone.View.extend({
    el: $('body'),
    ammoniaId: null,
    events: {
        "click .btn-delete-ammonia": "openAmmoniaModal",
        "click .btn-delete-ammonia-level": "deleteAmmonia"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openAmmoniaModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.ammoniaId = $btn.data('ammonia');

        // Open the modal
        $('.delete-ammonia-modal').modal('show');
    },
    deleteAmmonia: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/ammonia/delete/' + this.ammoniaId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
