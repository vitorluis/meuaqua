/**
 * Created by vitor on 06/01/17.
 */
var AmmoniaEditView = Backbone.View.extend({
    el: $('body'),
    ammoniaId: null,
    level: null,
    events: {
        "click .btn-edit-ammonia": "openAmmoniaModal",
        "click .btn-save-ammonia-level": "saveAmmonia",
        "submit #edit-ammonia": "saveAmmonia"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openAmmoniaModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.ammoniaId = $btn.data('ammonia');
        this.level = $btn.data('level');

        // Set the current level on input
        $('.ammonia-edit-level').val(this.level);

        // Open the modal
        $('.edit-ammonia-modal').modal('show');
    },
    saveAmmonia: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-ammonia')[0]);
        data.append('ammonia_id', this.ammoniaId);


        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/ammonia/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
