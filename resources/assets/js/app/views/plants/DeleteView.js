/**
 * Created by vitor on 06/01/17.
 */
var PlantsDeleteView = Backbone.View.extend({
    el: $('body'),
    plantId: null,
    events: {
        "click .btn-delete-plant": "openPlantModal",
        "click .btn-delete-plants": "deletePlants"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openPlantModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.plantId = $btn.data('plant');

        // Open the modal
        $('.delete-plant-modal').modal('show');
    },
    deletePlants: function () {
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.delete('/plants/delete/' + this.plantId, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function () {
        $('.error-msg').html('<li>Erro ao deletar, tente novamente</li>');
        $('.alert-error').show();
    }
});
