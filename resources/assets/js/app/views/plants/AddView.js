/**
 * Created by vitor on 06/01/17.
 */
var PlantsAddView = Backbone.View.extend({
    el: $('body'),
    plantType: null,
    quantity: null,
    aquarium: null,
    events: {
        "click .btn-add-plant": "openPlantModal",
        "click .btn-save-plants": "savePlants",
        "change .plant-type": "changeSelectedPlant",
        "submit #new-plant": "savePlants"
    },
    initialize: function () {
        this.changeSelectedPlant();

        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openPlantModal: function () {
        $('.new-plant-modal').modal('show');
    },
    savePlants: function (evt) {
        evt.preventDefault();
        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#new-plant')[0]);
        data.append('aquarium', this.aquarium);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/plants/save', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg').html(errors_str);
        $('.alert-error').show();
    },
    changeSelectedPlant: function () {
        var selected = $('.plant-type :selected').data('image');
        $('.selected-plant-type').attr('src', selected);
    }
});
