/**
 * Created by vitor on 06/01/17.
 */
var PlantsEditView = Backbone.View.extend({
    el: $('body'),
    plantId: null,
    quantity: null,
    events: {
        "click .btn-edit-plant": "openPlantModal",
        "click .btn-save-plant-amount": "savePlants",
        "submit #edit-plant": "savePlants"
    },
    initialize: function () {
        // Get the aquarium ID
        this.aquarium = $('.aquarium-id').val();
    },
    openPlantModal: function (evt) {
        // Get values from button
        var $btn = $(evt.currentTarget);
        this.plantId = $btn.data('plant');
        this.quantity = $btn.data('amount');

        // Set the current amount on input
        $('.plant-edit-quantity').val(this.quantity);

        // Open the modal
        $('.edit-plant-modal').modal('show');
    },
    savePlants: function (evt) {
        evt.preventDefault();

        // First of all, hide the alert;
        var $alertError = $('.alert-error');
        $alertError.hide();

        // Get the data
        var data = new FormData($('#edit-plant')[0]);
        data.append('plant_id', this.plantId);

        // Call WebService to save new aquarium
        var ws = new Webservice();
        ws.post('/plants/edit', data, this.successCallback, this.failCallback)
    },
    successCallback: function (data) {
        if (data.response) {
            window.location.reload();

        }
    },
    failCallback: function (data) {
        var errors = JSON.parse(data.responseText);
        var errors_str = '';
        for (var i in errors) {
            errors_str += '<li>' + errors[i][0] + '</li>';
        }

        $('.error-msg-edit').html(errors_str);
        $('.alert-error-edit').show();
    }
});
