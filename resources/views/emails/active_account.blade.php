<h2>Olá {{ $name }}, bem vindo ao Meu Aqua!!</h2>

<p>
    Para ativar a sua conta, por favor, clique no link a seguir: <a href="{{ $link }}">{{ $link }}</a>
</p>