<h2>Olá {{ $name }},</h2>

<p>
    Para trocar a sua senha, por favor, clique no link a seguir: <a href="{{ $link }}">{{ $link }}</a>
</p>