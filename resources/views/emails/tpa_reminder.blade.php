<h2>Olá {{ $user->name }},</h2>

<p>
    Você tem uma TPA agendada para amanhã ({{ $tpa->date }}).
</p>
<p>
    De acordo com os registros, a porcentagem a ser trocada é de <strong>{{ $tpa->percentage }}%</strong>.
</p>

<p>
    Tenha um ótimo dia. :D
</p>