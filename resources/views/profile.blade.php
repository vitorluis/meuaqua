@extends("layout.master")
@section("content")
    <div class="page-title">
        <h3>Profile</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="active">Profile</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <br/>
            @if(session('status'))
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        Por favor, corrija os erros abaixo: <br/><br/>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
        {!! Form::open(['url' => '/profile', 'class' => 'm-t-md form-profile', 'enctype' => "multipart/form-data"]) !!}
        <div class="col-sm-12 col-xs-12">
            <div class="col-sm-12 col-xs-12">
                <h2>Foto do Perfil</h2>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="profile-image-container">
                    <img src="@if ($user->photo == null) assets/images/no-avatar.png @else {{ $user_photo }} @endif"
                         alt="" class="profile-avatar">
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group @if($errors->has('photo')) has-error @endif">
                    {!! Form::file('photo', ['class' => 'form-control avatar-upload']) !!}
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="col-sm-12 col-xs-12">
                <h2>Dados básicos</h2>
            </div>
            <div class="form-group @if($errors->has('name')) has-error @endif col-xs-12 col-sm-12 ">
                {!! Form::text('name', $user->name, ['placeholder' => 'Nome', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group @if($errors->has('city')) has-error @endif col-xs-12 col-sm-5 ">
                {!! Form::text('city', $user->city, ['placeholder' => 'Cidade', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group @if($errors->has('state')) has-error @endif col-xs-12 col-sm-2 ">
                {!! Form::text('state', $user->state, ['placeholder' => 'Estado', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group @if($errors->has('phone')) has-error @endif col-xs-12 col-sm-9 ">
                {!! Form::text('phone', $user->phone, ['placeholder' => 'Telefone', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12">
            <div class="col-xs-12 col-sm-4">
                {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('scripts')
    <script src="assets/plugins/select2/js/select2.min.js"></script>
    <script>
        var profileView = new ProfileView();
    </script>
@stop