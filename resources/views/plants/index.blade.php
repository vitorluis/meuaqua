@extends("layout.master")
@section("content")
    <input type="hidden" value="{{ $aquarium->id }}" class="aquarium-id"/>
    <div class="page-title">
        <h3>Aquário</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></li>
                <li class="active">Minhas Plantas</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Minhas Plantas - <a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></h1>
            </div>
            <!-- Button to create new Aquarium -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <div class="form-group">
                            <button class="btn btn-success btn-add-plant"><i class="fa fa-plus"></i> Adicionar Plantas
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-middle-align">
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Nome</th>
                                    <th>Nome Científico</th>
                                    <th>Quantidade</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($aquarium->plants->count() == 0)
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            <h2>Nenhuma planta cadastrada</h2>
                                        </td>
                                    </tr>
                                @endif
                                @foreach($aquarium->plants as $plant)
                                    <tr>
                                        <td style="text-align: center">
                                            <img src="{{ $plant->type->photo }}"
                                                 class="plant-table-image">
                                        </td>
                                        <td><h1>{{ $plant->type->name }}</h1></td>
                                        <td><h1>{{ $plant->type->scientific_name }}</h1></td>
                                        <td><h1>{{ $plant->amount }}</h1></td>
                                        <td>
                                            <button class="btn btn-info btn-edit-plant" data-plant="{{ $plant->id }}"
                                                    data-amount="{{ $plant->amount }}"><i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-danger btn-delete-plant"
                                                    data-plant="{{ $plant->id }}">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for new plant -->
            <div class="modal fade new-plant-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Adicionar nova planta...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <form action="#" id="new-plant" method="POST">
                                    <div class="form-group">
                                        <div class="col-sm-5 col-xs-12"
                                             style="text-align: center; margin-bottom: 10px;">
                                            <img src="" class="selected-plant-type" width="100%"/>
                                        </div>
                                        <div class="col-xs-12 col-sm-7">
                                            <div class="col-sm-12">
                                                <div class="form-group">

                                                    <select class="form-control plant-type" name="plant_type">
                                                        @foreach($plants_types as $plant_type)
                                                            <option value="{{ $plant_type->id }}"
                                                                    data-image="{{ $plant_type->photo }}">{{ $plant_type->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type="text" class="plant-amount form-control" name="quantity"
                                                           placeholder="Quantidade">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-plants">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for change amount of plants -->
            <div class="modal fade edit-plant-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Editar quantidade...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error-edit"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg-edit">

                                        </ul>
                                    </div>
                                </div>
                                <form action="#" id="edit-plant" method="POST">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <input type="text" class="plant-edit-quantity form-control" name="quantity"
                                                   placeholder="Quantidade">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-plant-amount">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal to delete plant -->
            <div class="modal fade delete-plant-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Deletar Planta...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <p>Tem certeza que quer excluir essa planta?</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-delete-plants">Deletar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')
        <!-- Add scripts for this page -->
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
    <script>
        new PlantsAddView();
        new PlantsEditView();
        new PlantsDeleteView();
    </script>
@stop