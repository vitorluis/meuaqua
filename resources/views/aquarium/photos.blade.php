@extends("layout.master")
@section("content")
    <input type="hidden" value="{{ $aquarium->id }}" class="aquarium-id"/>
    <div class="page-title">
        <h3>Dashboard</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Meus Aquários</a></li>
                <li><a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Fotos do Aquário</h1>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <div class="form-group">
                            <button class="btn btn-success btn-add-photos"><i class="fa fa-plus"></i> Adicionar novas
                                fotos
                            </button>
                        </div>
                        <div class="form-group" id="photo-gallery">
                            @if ($aquarium->photos->count() == 0)
                                <h1 class="text-center">Nenhuma foto adicionada. :(</h1>
                            @else
                                @foreach($aquarium->photos as $photo)
                                    <img data-photo-id="{{ $photo->id }}" src="{{ $photo->path }}"
                                         class="aquarium-photo"/>
                                @endforeach
                            @endif
                        </div>
                        @if ($aquarium->photos->count() > 0)
                            <div class="row">
                                <div class="col-sm-3 col-xs-12 pull-right">
                                    <button class="btn btn-danger pull-right btn-delete-current-photo"
                                            data-photo-id="{{ $aquarium->photos->first()->id }}">
                                        <i class="fa fa-trash"></i> Excluir esta foto
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade add-photos modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myLargeModalLabel">Adicionar novas fotos...</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger alert-dismissible alert-error"
                                 style="display: none;" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <span class="error-message"></span>
                            </div>
                            <form action="#" enctype="multipart/form-data"
                                  class="photo-upload dropzone"></form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-send-photos">Salvar</button>
                    <button type="button" class="btn btn-default btn-cancel-photos" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal to delete kh -->
    <div class="modal fade delete-photo-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myLargeModalLabel">Deletar foto...</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger alert-dismissible alert-error"
                                 style="display: none;" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                Por favor, corrija os erros abaixo: <br/><br/>
                                <ul class="error-msg">

                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <p>Tem certeza que quer excluir essa foto? Você irá excluir a foto que está sendo
                                apresentada na galeria</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-delete-photo">Deletar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link href="/assets/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/unitegallery/css/unite-gallery.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/unitegallery/themes/default/ug-theme-default.css" rel="stylesheet" type="text/css"/>
@stop
@section('scripts')
    <!-- Add scripts for this page -->
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
    <script src="/assets/plugins/unitegallery/js/unitegallery.js" type="text/javascript"></script>
    <script src="/assets/plugins/unitegallery/themes/default/ug-theme-default.js" type="text/javascript"></script>
    <script src="/assets/plugins/unitegallery/themes/compact/ug-theme-compact.js" type="text/javascript"></script>
    <script>
        new AquariumPhotosView();
    </script>
@stop