@extends("layout.master")
@section("content")
    <div class="page-title">
        <h3>Dashboard</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Meus Aquários</a></li>
                <li><a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></li>
                <li class="active">Visão Geral</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Visão Geral</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/fishes/' . $aquarium->id) }}">
                            <h2 class="pull-left">Fauna: {{ $fishes }} peixes</h2>
                            <img src="/assets/images/home-icons/fauna.jpg"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/plants/' . $aquarium->id) }}">
                            <h2 class="pull-left">Flora: {{ $plants }} plantas</h2>
                            <img src="/assets/images/home-icons/flora.jpg"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/tpa/' . $aquarium->id) }}">
                            <h2 class="pull-left">Última TPA: {{ $last_tpa }} dias</h2>
                            <img src="/assets/images/home-icons/ultima-tpa.jpg"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/tpa/' . $aquarium->id) }}">
                            <h2 class="pull-left">Próxima TPA: {{ $next_tpa }} dias</h2>
                            <img src="/assets/images/home-icons/proxima-tpa.jpg"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/aquarium/photos/' . $aquarium->id) }}">
                            <h2 class="pull-left">Fotos: {{ $aquarium->photos->count() }} fotos</h2>
                            <img src="/assets/images/home-icons/camera-flat.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Paramêtros</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/temperature/' . $aquarium->id) }}">
                            @if($aquarium->temperature->count() > 0)
                                <h2 class="pull-left">Temp.: {{ $aquarium->temperature->first()->degrees }}ºC</h2>
                            @else
                                <h2 class="pull-left">Temp.: 0</h2>
                            @endif
                            <img src="/assets/images/home-icons/termometro.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/ph/' . $aquarium->id) }}">
                            @if($aquarium->ph->count() > 0)
                                <h2 class="pull-left">pH: {{ $aquarium->ph->first()->level }}</h2>
                            @else
                                <h2 class="pull-left">pH: 0</h2>
                            @endif
                            <img src="/assets/images/home-icons/ph.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/ammonia/' . $aquarium->id) }}">
                            @if($aquarium->ammonia->count() > 0)
                                <h2 class="pull-left">Amônia: {{ $aquarium->ammonia->first()->level }}</h2>
                            @else
                                <h2 class="pull-left">Amônia: 0</h2>
                            @endif
                            <img src="/assets/images/home-icons/amonia.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/nitrite/' . $aquarium->id) }}">
                            @if($aquarium->nitrite->count() > 0)
                                <h2 class="pull-left">Nitrito: {{ $aquarium->nitrite->first()->level }}</h2>
                            @else
                                <h2 class="pull-left">Nitrito: 0</h2>
                            @endif
                            <img src="/assets/images/home-icons/nitrito.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/gh/' . $aquarium->id) }}">
                            @if($aquarium->gh->count() > 0)
                                <h2 class="pull-left">GH: {{ $aquarium->gh->first()->level }} dH</h2>
                            @else
                                <h2 class="pull-left">GH: 0 dH</h2>
                            @endif
                            <img src="/assets/images/home-icons/gh.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <a href="{{ url('/kh/' . $aquarium->id) }}">
                            @if($aquarium->kh->count() > 0)
                                <h2 class="pull-left">KH: {{ $aquarium->kh->first()->level }} dH</h2>
                            @else
                                <h2 class="pull-left">KH: 0 dH</h2>
                            @endif
                            <img src="/assets/images/home-icons/kh.png"
                                 class="img-circle img-responsive pull-right img-force-width-80">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <!-- Add scripts for this page -->
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
@stop