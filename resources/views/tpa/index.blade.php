@extends("layout.master")
@section("content")
    <input type="hidden" value="{{ $aquarium->id }}" class="aquarium-id"/>
    <div class="page-title">
        <h3>Aquário</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></li>
                <li class="active">Trocas Parciais de Água</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Trocas Parciais de Água - <a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></h1>
            </div>
            <!-- Table with next TPAs -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Próximas TPAs</h3>
                    </div>
                    <div class="panel-body info-box">
                        <div class="form-group">
                            <button class="btn btn-success btn-add-tpa"><i class="fa fa-plus"></i> Adicionar nova TPA
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-middle-align data-table next-tpa-table">
                                <thead>
                                <tr>
                                    <th>Data de execução</th>
                                    <th>Porcentagem</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($next_tpa as $tpa)
                                    <tr>
                                        <td><span>{{ $tpa->date }}</span></td>
                                        <td><span>{{ $tpa->percentage }}%</span></td>
                                        <td align="center" class="action-buttons">
                                            <button class="btn btn-info btn-edit-tpa btn-sm" data-tpa="{{ $tpa->id }}"
                                                    data-date="{{ $tpa->date }}" data-perc="{{ $tpa->percentage }}"><i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-danger btn-delete-tpa btn-sm"
                                                    data-tpa="{{ $tpa->id }}">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Table with last TPAs-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Últimas TPAs</h3>
                    </div>
                    <div class="panel-body info-box">
                        <div class="form-group">
                            <button class="btn btn-success btn-add-tpa"><i class="fa fa-plus"></i> Adicionar nova TPA
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-middle-align data-table last-tpa-table">
                                <thead>
                                <tr>
                                    <th>Data de execução</th>
                                    <th>Porcentagem</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($last_tpa as $tpa)
                                    <tr>
                                        <td><span>{{ $tpa->date }}</span></td>
                                        <td><span>{{ $tpa->percentage }}%</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for new tpa -->
            <div class="modal fade new-tpa-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Adicionar nova TPA...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <form action="#" id="new-tpa" method="POST">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <input type="text" class="tpa-date form-control" name="date" readonly
                                                       placeholder="Data de execução">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="tpa-perc form-control" name="percentage"
                                                       placeholder="Porcentagem">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-tpa">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for change level of tpa -->
            <div class="modal fade edit-tpa-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Editar TPA...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error-edit"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg-edit">

                                        </ul>
                                    </div>
                                </div>
                                <form action="#" id="edit-tpa" method="POST">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <input type="text" class="tpa-edit-date form-control" name="date" readonly
                                                       placeholder="Data de execução">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="tpa-edit-perc form-control" name="percentage"
                                                       placeholder="Porcentagem">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-tpa-date">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal to delete tpa -->
            <div class="modal fade delete-tpa-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Deletar TPA...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <p>Tem certeza que quer excluir essa TPA?</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-delete-tpa-level">Deletar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link href="/assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
@stop
@section('scripts')
    <!-- Add scripts for this page -->
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
    <script src="/assets/plugins/datatables/js/jquery.datatables.min.js"></script>
    <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script>
        new TPAAddView();
        new TPAEditView();
        new TPADeleteView();
    </script>
@stop