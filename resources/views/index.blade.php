@extends("layout.master")
@section("content")
    <div class="page-title">
        <h3>Dashboard</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="active">Meus Aquários</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Meus Aquários</h1>
            </div>
            @foreach($aquariums as $aquarium)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="panel panel-border">
                        <a href="{{ url('/aquarium/' . $aquarium->id) }}">
                            <div class="zoomed-background square-aquarium @if ($aquarium->recent_photo == null) fade-background @endif"
                                 style="background-image: url('@if ($aquarium->recent_photo == null) /assets/images/aquarium-no-photo.jpg @else {{ $aquarium->recent_photo }} @endif');">

                            </div>
                        </a>
                        <div class="panel-body info-box">
                            <a href="{{ url('/aquarium/' . $aquarium->id) }}"><h2
                                        class="pull-left">{{ $aquarium->name }}</h2></a>

                            <div class="pull-right" style="margin-top: 20px;">
                                <a class="btn btn-warning" href="{{ url('/aquarium/photos/'. $aquarium->id) }}"><i
                                            class="fa fa-camera-retro"></i> Fotos
                                </a>
                                <button class="btn btn-info btn-edit-aquarium" data-aquarium="{{ $aquarium->id }}"><i
                                            class="fa fa-edit"></i> Editar
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach

        <!-- Button to create new Aquarium -->
            @if ($user->premium || $aquariums->count() < 2)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="panel panel-border">
                        <div class="panel-body info-box">
                            <a href="#" style="text-decoration: none" class="new-aquarium-btn">
                                <h1 class="text-center">
                                    <i class="fa fa-plus-circle fa-4x"></i>
                                </h1>

                                <h1 class="text-center">
                                    Adicionar novo Aquário
                                </h1>
                            </a>
                        </div>
                    </div>
                </div>
        @endif

        <!-- Modal for new Aquarium -->
            <div class="modal fade new-aquarium-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Adicionar novo aquário...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>

                                <form action="#" enctype="multipart/form-data" id="new-aquarium">
                                    <div class="col-xs-12">

                                        <div class="col-xs-12 col-sm-6 form-group">
                                            <input type="text" class="aquarium-name form-control"
                                                   placeholder="Nome" name="name">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 form-group">
                                            <select class="form-control aquarium-water-type" name="water_type">
                                                <option value="doce">Água Doce</option>
                                                <option value="salgada">Água Salgada</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-6 col-sm-3">
                                            <input type="text" class="aquarium-length form-control" name="length"
                                                   placeholder="Comprimento">
                                        </div>
                                        <div class="col-xs-6 col-sm-3">
                                            <div class="form-group">
                                                <input type="text" class="aquarium-width form-control" name="width"
                                                       placeholder="Largura">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-3">
                                            <input type="text" class="aquarium-height form-control" name="height"
                                                   placeholder="Altura">
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6 col-sm-3">
                                                <input type="text" class="aquarium-liters form-control" name="liters"
                                                       placeholder="Litros">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <textarea class="aquarium-description form-control" placeholder="Descrição"
                                                      name="description"
                                                      rows="5"></textarea>
                                        </div>

                                    </div>
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <label>Foto de capa:</label>
                                            <input type="file" class="aquarium-photo form-control" name="photo"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-aquarium">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for edit aquarium -->
            <div class="modal fade edit-aquarium-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Editar aquário...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error-edit"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg-edit">

                                        </ul>
                                    </div>
                                </div>

                                <form action="#" enctype="multipart/form-data" id="edit-aquarium">
                                    <div class="col-xs-12 form-group">

                                        <div class="col-xs-12 col-sm-6">
                                            <input type="text" class="aquarium-name form-control"
                                                   placeholder="Nome" name="name">
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <select class="form-control aquarium-water-type" name="water_type">
                                                <option value="doce">Água Doce</option>
                                                <option value="salgada">Água Salgada</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-4 col-sm-3">
                                            <input type="text" class="aquarium-length form-control" name="length"
                                                   placeholder="Comprimento">
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <input type="text" class="aquarium-width form-control" name="width"
                                                   placeholder="Largura">
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <input type="text" class="aquarium-height form-control" name="height"
                                                   placeholder="Altura">
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <input type="text" class="aquarium-liters form-control" name="liters"
                                                   placeholder="Litros">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <textarea class="aquarium-description form-control" placeholder="Descrição"
                                                      name="description"
                                                      rows="5"></textarea>
                                        </div>

                                    </div>
                                    <div class="col-xs-12 form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <label>Foto de capa:</label>
                                            <input type="file" class="aquarium-photo form-control" name="photo"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-edit-aquarium">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal to delete fish -->
            <div class="modal fade delete-aquarium-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Deletar Peixe...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <p>Tem certeza que quer excluir esse aquário?</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-delete-aquarium">Deletar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <!-- Add scripts for this page -->
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
    <script>
        var aquariumAdd = new AquariumAddView();
        var aquariumEdit = new AquariumEditView();
    </script>
@stop