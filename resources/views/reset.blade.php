<!DOCTYPE html>
<html>
<head>

    <!-- Title -->
    <title>Meu Aqua | Resetar senha</title>

    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Meu Aqua"/>
    <meta name="keywords" content="aquario, controle, parametros, aquario controle"/>
    <meta name="author" content="Vitor Villar"/>

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
    <link href="/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/themes/blue.css" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css"/>

    <script src="/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
    <script src="/assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-forgot">
<main class="page-content">
    <div class="page-inner">
        <div id="main-wrapper">
            <div class="row">
                <div class="col-md-3 center">
                    <div class="login-box" style="background-color: white; padding: 20px;">
                        <a href="{{ url('/') }}" class="logo-name text-lg text-center" style="margin-bottom: 10px;">
                            <img src="/assets/images/ma-logo.png" width="300"/>
                        </a>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                Por favor, corrija os erros abaixo: <br/><br/>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('error_reset'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                {{ session('error_reset') }}
                            </div>
                        @endif
                        <h2 class="text-center">Troca de senha</h2>
                        <p class="text-center m-t-md">Entra com a sua nova senha.</p>
                        {!! Form::open(['url' => '/reset', 'class' => 'm-t-md']) !!}
                        {!! Form::hidden('token', $user->reset_password_token) !!}
                        {!! Form::hidden('user_id', $user->id) !!}
                        <div class="form-group">
                            {!! Form::password('password', ['placeholder' => 'Senha', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::password('password_confirmation', ['placeholder' => 'Confirme a senha', 'class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-block']) !!}
                        <a href="{{ url('/') }}" class="btn btn-default btn-block m-t-md">Voltar</a>
                        {!! Form::close() !!}
                        <p class="text-center m-t-xs text-sm">2017 &copy; Meu Aqua.</p>
                    </div>
                </div>
            </div><!-- Row -->
        </div><!-- Main Wrapper -->
    </div><!-- Page Inner -->
</main><!-- Page Content -->


<!-- Javascripts -->
<script src="/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/plugins/pace-master/pace.min.js"></script>
<script src="/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/switchery/switchery.min.js"></script>
<script src="/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="/assets/plugins/offcanvasmenueffects/js/classie.js"></script>
<script src="/assets/plugins/waves/waves.min.js"></script>
<script src="/assets/js/modern.min.js"></script>

</body>
</html>