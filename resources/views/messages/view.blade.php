@extends("layout.master")
@section("content")
    <div class="col-md-12" style="margin-top: 1%;">
        <div class="row mailbox-header">
            <div class="col-md-8">
                <h2>Mensagem</h2>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="mailbox-content">
            <div class="message-header">
                <h3><span>Assunto:</span> {{ $mail->subject }}</h3>
            </div>
            @foreach($messages as $message)
                <div class="message-sender">
                    <img src="@if($message->sender->photo != null) {{ $message->sender->photo }} @else /assets/images/no-avatar.png @endif"
                         alt="">

                    <p>{{ $message->sender->name }}</p>

                    <p class="message-date pull-right">{{ \Carbon\Carbon::createFromTimestamp($message->send_at)->format('d F Y') }}</p>
                </div>
                <div class="message-content" style="margin-bottom: 2%">
                    <p>{!!  $message->message !!} </p>
                </div>
            @endforeach
            <div class="message-options pull-right">
                <a href="#" class="btn btn-success reply-message" data-mail-id="{{ $mail->id }}" data-sender-id="{{ $user->id }}"
                            data-receiver-id="{!! $user->id == $mail->sender_id ? $mail->receiver_id : $mail->sender_id !!}"><i
                            class="fa fa-reply m-r-xs"></i>Responder</a>
                <a href="#" class="btn btn-default"><i class="fa fa-trash m-r-xs"></i>Excluir</a>
            </div>
        </div>
    </div>
    <div class="reply-message-area"  style="display: none">
        <div class="col-sm-8 col-xs-12">
            <h2>Responder Mensagem</h2>
        </div>
        <div class="col-sm-12">
            <textarea class="user-message-reply" title="Responder Mensagem"></textarea>
        </div>
        <div class="col-sm-12 col-xs-12" style="margin-top: 1%; margin-bottom: 2%;">
            <button class="btn btn-default cancel-reply pull-right"><i class="fa fa-times"></i> Cancelar</button>
            <button class="btn btn-warning send-reply pull-right" style="margin-right: 0.2%;"><i class="fa fa-send"></i> Enviar</button>
        </div>
    </div>
@endsection
@section("css")
    <link href="/assets/plugins/summernote-master/summernote.css" rel="stylesheet" type="text/css"/>
@endsection
@section("scripts")
    <script src="/assets/plugins/summernote-master/summernote.min.js"></script>
    <script src="/assets/js/app/views/MessageView.js"></script>
    <script>
        var messageView = new MessageView();
    </script>
@endsection