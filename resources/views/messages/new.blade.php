@extends("layout.master")
@section("content")
    <div class="col-sm-12">
        <div class="row m-t-md">
            <div class="col-md-12">
                <div class="row mailbox-header">
                    <div class="col-md-2">
                        <h1>Mensagem</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                {!! Form::open(['url' => '/messages/send', 'class' => 'form-horizontal']) !!}
                <div class="mailbox-content">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            Por favor, corrija os erros abaixo: <br/><br/>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="compose-body">
                        {!! Form::hidden('user_id', $user->id) !!}
                        <div class="form-group">
                            <label for="to" class="col-sm-2 control-label">Para</label>

                            <div class="col-sm-10">
                                {!! Form::text('to', $user->name, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">Assunto</label>

                            <div class="col-sm-10">
                                {!! Form::text('subject', old('subject'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="compose-message">
                        <div class="wrap-summernote" style="background-color: white">
                            {!! Form::textarea('message', old('message'), ['class' => 'summernote']) !!}
                        </div>
                    </div>
                    <div class="compose-options">
                        <div class="pull-right">
                            {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- Row -->
    </div>
@endsection
@section("css")
    <link href="/assets/plugins/summernote-master/summernote.css" rel="stylesheet" type="text/css"/>
@endsection
@section("scripts")
    <script src="/assets/plugins/summernote-master/summernote.min.js"></script>
    <script>
        $('.summernote').summernote({
            height: 350
        });
    </script>
@endsection