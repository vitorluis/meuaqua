@extends("layout.master")
@section("content")
    <div class="col-sm-12">
        <div class="row m-t-md">
            <div class="col-md-12">
                <div class="row mailbox-header">
                    <div class="col-md-12">
                        <h1>Mensagens</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="mailbox-content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th colspan="1" class="hidden-xs">
                                <span><input type="checkbox" class="check-mail-all"></span>
                            </th>
                            <th class="text-right" colspan="5">
                                <span class="text-muted m-r-sm">Showing 20 of 346 </span>
                                <a class="btn btn-default m-r-sm" data-toggle="tooltip" data-placement="top"
                                   title="Refresh"><i class="fa fa-refresh"></i></a>

                                <div class="btn-group m-r-sm mail-hidden-options">
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                       title="Delete"><i class="fa fa-trash"></i></a>
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                       title="Report Spam"><i class="fa fa-exclamation-circle"></i></a>
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                       title="Mark as Important"><i class="fa fa-star"></i></a>
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                       title="Mark as Read"><i class="fa fa-pencil"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default"><i class="fa fa-angle-left"></i></a>
                                    <a class="btn btn-default"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mails as $mail)
                            <tr class="@if (($mail->sender_id == $user->id && $mail->sender_has_new_message) || ($mail->receiver_id == $user->id && $mail->receiver_has_new_message)) unread @else read @endif"
                                data-mail-id="{{ $mail->id }}">
                                <td class="hidden-xs">
                                    <span><input type="checkbox" class="checkbox-mail"></span>
                                </td>
                                <td>
                                    <i class="fa fa-star icon-state-warning"></i>
                                </td>
                                <td class="hidden-xs">
                                    @if ($user->id == $mail->sender->id)
                                        {{ $mail->receiver->name }}
                                    @else
                                        {{ $mail->sender->name }}
                                    @endif
                                </td>
                                <td>
                                    {{ $mail->subject }}
                                </td>
                                <td>
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::createFromTimestamp($mail->last_message)->format('d F Y') }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="assets/js/app/views/InboxView.js"></script>
    <script>
        var inboxView = new InboxView();
    </script>
@stop