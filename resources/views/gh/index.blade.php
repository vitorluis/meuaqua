@extends("layout.master")
@section("content")
    <input type="hidden" value="{{ $aquarium->id }}" class="aquarium-id"/>
    <div class="page-title">
        <h3>Aquário</h3>

        <div class="page-breadcrumb">
            <ol class="breadcrumb">

                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></li>
                <li class="active">Níveis de GH</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <h1>Nível de GH - <a href="{{ url('/aquarium/' . $aquarium->id) }}">{{ $aquarium->name }}</a></h1>
            </div>
            <!-- Button to create new Aquarium -->
            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-body info-box">
                        <div class="form-group">
                            <button class="btn btn-success btn-add-gh"><i class="fa fa-plus"></i> Adicionar novo nível
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-middle-align data-table gh-table">
                                <thead>
                                <tr>
                                    <th>Nível</th>
                                    <th>Data de inserção</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($aquarium->gh as $gh)
                                    <tr data-level="{{ $gh->level }}" data-measure="{{ $gh->measure_date }}">
                                        <td><span>{{ $gh->level }} dH</span></td>
                                        <td><span>{{ $gh->measure_date }}</span></td>
                                        <td align="center" class="action-buttons">
                                            <button class="btn btn-info btn-edit-gh btn-sm" data-gh="{{ $gh->id }}"
                                                    data-level="{{ $gh->level }}"><i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-danger btn-delete-gh btn-sm"
                                                    data-gh="{{ $gh->id }}">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Graghic --}}
            <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="panel panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Histórico do GH</h3>
                    </div>
                    <div class="panel-body info-box">
                        <div>
                            <canvas id="chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for new gh -->
            <div class="modal fade new-gh-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Adicionar novo nível de GH...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <form action="#" id="new-gh" method="POST">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="form-group">
                                                <input type="text" class="gh-level form-control" name="level"
                                                       placeholder="Nível">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-gh">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for change level of gh -->
            <div class="modal fade edit-gh-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Editar nível...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error-edit"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg-edit">

                                        </ul>
                                    </div>
                                </div>
                                <form action="#" id="edit-gh" method="POST">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12">
                                            <input type="text" class="gh-edit-level form-control" name="level"
                                                   placeholder="Nível">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-save-gh-level">Salvar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal to delete gh -->
            <div class="modal fade delete-gh-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="myLargeModalLabel">Deletar Nível de GH...</h2>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger alert-dismissible alert-error"
                                         style="display: none;" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        Por favor, corrija os erros abaixo: <br/><br/>
                                        <ul class="error-msg">

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <p>Tem certeza que quer excluir esse GH?</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-delete-gh-level">Deletar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link href="/assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>
@stop
@section('scripts')
    <!-- Add scripts for this page -->
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
    <script src="/assets/plugins/datatables/js/jquery.datatables.min.js"></script>
    <script src="/assets/plugins/chartsjs/Chart.min.js"></script>
    <script>
        new GhAddView();
        new GhEditView();
        new GhDeleteView();
        new GhChartView();
    </script>
@stop