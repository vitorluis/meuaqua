<!DOCTYPE html>
<html>
<head>

    <!-- Title -->
    <title>Meu Aqua | Registrar</title>

    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Admin Dashboard Template" />
    <meta name="keywords" content="admin,dashboard" />
    <meta name="author" content="Vitor Villar" />

    <!-- Styles -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
    <link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/labelholder.min.css" rel="stylesheet" type="text/css"/>

    <script src="assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
    <script src="assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-register login-alt">
<main class="page-content">
    <div class="page-inner">
        <div id="main-wrapper">
            <div class="row">
                <div class="col-md-6 center">
                    <div class="login-box panel panel-white">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ url('/') }}" class="logo-name text-lg">
                                        <img src="assets/images/ma-logo.png" width="300" style="border-radius: 20px"/>
                                    </a>
                                    <p class="m-t-md">
                                        Meu Aqua é uma aplicação que te ajuda a controlar todos os parametros do seu aquário.
                                        Desde aquários mais simples até aquários mais complexos, tanto de água doce e água salgada.
                                    </p>

                                    <p class="m-t-md">
                                        Além de manter um histórico de paramêtros, você também pode manter um histórico visual
                                        de seu aquário.
                                    </p>
                                    <img src="assets/images/aqua-logo.jpg" width="100%" class="border-corner grey-border"/>
                                    <p class="m-t-xs text-sm">2016 &copy; Meu Aqua.</p>
                                </div>
                                <div class="col-md-6">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            Por favor, corrija os erros abaixo: <br/><br/>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {!! Form::open(['url' => '/register', 'class' => 'm-t-md form-register']) !!}
                                        {!! Form::hidden('user_latitude', '', ['class' => 'user-latitude']) !!}
                                        {!! Form::hidden('user_longitude', '', ['class' => 'user-longitude']) !!}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group labelholder @if($errors->has('name')) has-error @endif">
                                            {!! Form::text('name', old('name'), ['placeholder' => 'Nome', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group labelholder @if($errors->has('email')) has-error @endif">
                                            {!! Form::email('email', old('email'), ['placeholder' => 'E-mail', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group labelholder @if($errors->has('password')) has-error @endif" data-label="Senha">
                                            {!! Form::password('password', ['placeholder' => 'Senha', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group labelholder @if($errors->has('password_confirmation')) has-error @endif" data-label="Confirme a senha">
                                            {!! Form::password('password_confirmation', ['placeholder' => 'Confirme a senha', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="additional-fields">
                                            <div class="form-group labelholder col-md-8 no-padding @if($errors->has('city')) has-error @endif">
                                                {!! Form::text('city', old('city'), ['placeholder' => 'Cidade', 'class' => 'form-control']) !!}
                                            </div>
                                            <div class="form-group labelholder col-md-4 no-padding-right @if($errors->has('state')) has-error @endif">
                                                {!! Form::text('state', old('state'), ['placeholder' => 'Estado', 'class' => 'form-control', 'maxlength' => 2]) !!}
                                            </div>
                                            <div class="form-group labelholder @if($errors->has('phone')) has-error @endif">
                                                {!! Form::text('phone', old('phone'), ['placeholder' => 'Telefone', 'class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                        <label>
                                            {!! Form::checkbox('accept_terms') !!} Eu aceito as pol&iacute;ticas e os termos de uso.
                                        </label>
                                        {!! Form::submit('Enviar', ['class' => 'btn btn-success btn-block m-t-xs']) !!}
                                        <p class="text-center m-t-xs text-sm">J&aacute; tem uma conta?</p>
                                        <a href="{{ url('/login') }}" class="btn btn-default btn-block m-t-xs">Login</a>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Row -->
        </div><!-- Main Wrapper -->
    </div><!-- Page Inner -->
</main><!-- Page Content -->

<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="assets/plugins/pace-master/pace.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/plugins/switchery/switchery.min.js"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="assets/plugins/offcanvasmenueffects/js/classie.js"></script>
<script src="assets/plugins/waves/waves.min.js"></script>
<script src="assets/js/modern.min.js"></script>
<script src="assets/js/libs/labelholder.min.js"></script>

<!-- Add the frameworks -->
<script src="assets/js/frameworks/underscore.min.js"></script>
<script src="assets/js/frameworks/backbone.min.js"></script>

<script>
    $(document).ready(function () {
        // Initialize the Register view
        var view = new RegisterView();
    })
</script>

</body>
</html>