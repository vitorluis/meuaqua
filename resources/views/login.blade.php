<!DOCTYPE html>
<html>
<head>

    <!-- Title -->
    <title>Meu Aqua | Login</title>

    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Admin Dashboard Template"/>
    <meta name="keywords" content="admin,dashboard"/>
    <meta name="author" content="Steelcoders"/>

    <!-- Styles -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
    <link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/labelholder.min.css" rel="stylesheet" type="text/css"/>

    <script src="assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
    <script src="assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-login login-alt">
<main class="page-content">
    <div class="page-inner">
        <div id="main-wrapper">
            <div class="row">
                <div class="col-md-6 center">
                    <div class="login-box panel panel-white">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ url('/') }}" class="logo-name text-lg">
                                        <img src="assets/images/ma-logo.png" width="300" style="border-radius: 20px"/>
                                    </a>

                                    <p class="m-t-md login-info">
                                        Meu Aqua é uma aplicação que te ajuda a controlar todos os parametros do seu aquário.
                                        Desde aquários mais simples até aquários mais complexos, tanto de água doce e água salgada.
                                    </p>

                                    <p class="m-t-md login-info">
                                        Além de manter um histórico de paramêtros, você também pode manter um histórico visual
                                        de seu aquário.
                                    </p>
                                    <img src="assets/images/aqua-logo.jpg" width="100%" class="border-corner grey-border"/>
                                    <p class="m-t-xs text-sm">2016 &copy; Meu Aqua.</p>

                                    <div class="btn-group btn-group-justified m-t-sm hidden" role="group"
                                         aria-label="Justified button group">
                                        <a href="#" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                                        <a href="#" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
                                        <a href="#" class="btn btn-google"><i class="fa fa-google-plus"></i> Google+</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if(session('error_login'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            {{ session('error_login') }}
                                        </div>
                                    @endif
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            Por favor, corrija os erros abaixo: <br/><br/>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {!! Form::open(['url' => '/login']) !!}
                                    <div class="form-group labelholder">
                                        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                                    </div>
                                    <div class="form-group labelholder">
                                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
                                    </div>
                                    {!! Form::submit('Login', ['class' => 'btn btn-success btn-block']) !!}
                                    <a href="{{ url('/forgot') }}" class="display-block text-center m-t-md text-sm">Esqueceu
                                        sua senha?</a>

                                        <p class="text-center m-t-xs text-sm">N&atilde;o tem uma conta?</p>
                                    <a href="{{ url('/register') }}" class="btn btn-default btn-block m-t-md">Criar uma
                                        nova Conta</a>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>
        <!-- Main Wrapper -->
    </div>
    <!-- Page Inner -->
</main>
<!-- Page Content -->


<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="assets/plugins/pace-master/pace.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/plugins/switchery/switchery.min.js"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="assets/plugins/offcanvasmenueffects/js/classie.js"></script>
<script src="assets/plugins/waves/waves.min.js"></script>
<script src="assets/js/modern.min.js"></script>
<script src="assets/js/libs/labelholder.min.js"></script>
<script>
</script>

</body>
</html>