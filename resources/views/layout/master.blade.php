<!DOCTYPE html>
<html>
<head>

    <!-- Title -->
    <title>Meu Aqua</title>

    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Meu Aqua - Controle de Aquário"/>
    <meta name="keywords" content="aquario, controle, parametros, aquario controle"/>

    <!-- Styles -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
    <link href="/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/themes/blue.css" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/labelholder.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>

    @yield("css")

    <script src="/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
    <script src="/assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-header-fixed">
@include("layout.right-sidebar")
@include("layout.search")
<main class="page-content content-wrap">
    @include("layout.navigation")
    @include("layout.sidebar")
    <div class="page-inner full-height">
        @yield("content")
        <div class="page-footer">
            <p class="no-s">2016 &copy; Vitor Villar.</p>
        </div>
    </div>
    <!-- Page Inner -->
</main>
<!-- Page Content -->
<div class="cd-overlay"></div>


<!-- Javascripts -->
<script src="/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="/assets/plugins/pace-master/pace.min.js"></script>
<script src="/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/switchery/switchery.min.js"></script>
<script src="/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="/assets/plugins/offcanvasmenueffects/js/classie.js"></script>
<script src="/assets/plugins/offcanvasmenueffects/js/main.js"></script>
<script src="/assets/plugins/waves/waves.min.js"></script>
<script src="/assets/plugins/3d-bold-navigation/js/main.js"></script>
<script src="/assets/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/assets/plugins/jquery-counterup/jquery.counterup.min.js"></script>
<script src="/assets/plugins/toastr/toastr.min.js"></script>
<script src="/assets/plugins/flot/jquery.flot.min.js"></script>
<script src="/assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="/assets/plugins/flot/jquery.flot.symbol.min.js"></script>
<script src="/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="/assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/assets/plugins/curvedlines/curvedLines.js"></script>
<script src="/assets/plugins/metrojs/MetroJs.min.js"></script>
<script src="/assets/js/modern.js"></script>

<!-- Add app Javascript -->
<script src="/assets/js/app.min.js"></script>

@yield('scripts')


</body>
</html>