<div class="page-sidebar sidebar">
    <div class="page-sidebar-inner slimscroll">
        <div class="sidebar-header">
            <div class="sidebar-profile">
                <a href="javascript:void(0);" id="profile-menu-link-a">
                    <div class="sidebar-profile-image">
                        {{--<img src="{{ $user_photo }}" class="img-circle img-responsive" alt="">--}}
                        <img src="{{ $user_photo }}" class="img-circle img-responsive" alt="">
                    </div>
                    <div class="sidebar-profile-details">
                        {{--<span>{{ $user_name }}</span>--}}
                        <span>{{ $user_name }}</span>
                    </div>
                </a>
            </div>
        </div>
        <ul class="menu accordion-menu">
            <li class="@if (Request::is('/')) active @endif">
                <a href="{{ url('/') }}" class="waves-effect waves-button">
                    <span class="menu-icon glyphicon glyphicon-home"></span>

                    <p>Home</p>
                </a>
            </li>
            <li class="@if (Request::is('profile')) active @endif">
                <a href="{{ url('/profile') }}" class="waves-effect waves-button">
                    <span class="menu-icon glyphicon glyphicon-user"></span>

                    <p>Perfil</p>
                </a>
            </li>
            <li class="@if (Request::is('aquarium*') || Request::is('fishes*') || Request::is('plants*')|| Request::is('ph*') || Request::is('temperature*')|| Request::is('ammonia*')|| Request::is('nitrite*') || Request::is('tpa*') || Request::is('gh*') || Request::is('kh*'))) active @endif">
                <a href="{{ url('/') }}" class="waves-effect waves-button">
                    <span class="menu-icon glyphicon glyphicon-tower"></span>

                    <p>Aquários</p>
                </a>
            </li>
        <!--
            <li class="@if (Request::is('alerts*')) active @endif">
                <a href="{{ url('/alerts') }}" class="waves-effect waves-button">
                    <span class="menu-icon glyphicon glyphicon-envelope"></span>

                    <p>Alertas</p>
                </a>
            </li>
            -->
            <!--<li class="droplink"><a href="#" class="waves-effect waves-button"><span
                            class="menu-icon glyphicon glyphicon-envelope"></span>

                    <p>Mailbox</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="inbox.html">Inbox</a></li>
                    <li><a href="inbox-alt.html">Inbox Alt</a></li>
                    <li><a href="message-view.html">View Message</a></li>
                    <li><a href="message-view-alt.html">View Message Alt</a></li>
                    <li><a href="compose.html">Compose</a></li>
                    <li><a href="compose-alt.html">Compose Alt</a></li>
                </ul>
            </li>-->
            <!--<li class="droplink"><a href="#" class="waves-effect waves-button"><span
                            class="menu-icon glyphicon glyphicon-flash"></span>

                    <p>Levels</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li class="droplink"><a href="#"><p>Level 1.1</p><span class="arrow"></span></a>
                        <ul class="sub-menu">
                            <li class="droplink"><a href="#"><p>Level 2.1</p><span class="arrow"></span></a>
                                <ul class="sub-menu">
                                    <li><a href="#">Level 3.1</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Level 2.2</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Level 1.2</a></li>
                </ul>
            </li>-->
        </ul>
    </div>
    <!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->