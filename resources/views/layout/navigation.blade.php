<div class="navbar">
    <div class="navbar-inner">
        <div class="sidebar-pusher">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="logo-box">
            <a href="{{ url('/') }}" class="logo-text">
                <img class="hidden-xs" src="/assets/images/ma-logo-text-white.png" width="130"/>
                <img class="hidden-sm hidden-md hidden-lg" src="/assets/images/ma-logo-text-blue.png" width="130"/>
            </a>
        </div>
        <!-- Logo Box -->
        <div class="topmenu-outer">
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i
                                    class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i
                                    class="fa fa-expand"></i></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic"
                           data-toggle="dropdown"><i class="fa fa-bell"></i><span
                                    class="badge badge-success pull-right">3</span></a>
                        <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                            <li><p class="drop-title">You have 3 pending tasks !</p></li>
                            <li class="dropdown-menu-list slimscroll tasks">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">
                                            <div class="task-icon badge badge-success"><i class="icon-user"></i></div>
                                            <span class="badge badge-roundless badge-default pull-right">1min ago</span>

                                            <p class="task-details">New user registered.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="task-icon badge badge-danger"><i class="icon-energy"></i></div>
                                            <span class="badge badge-roundless badge-default pull-right">24min ago</span>

                                            <p class="task-details">Database error.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="task-icon badge badge-info"><i class="icon-heart"></i></div>
                                            <span class="badge badge-roundless badge-default pull-right">1h ago</span>

                                            <p class="task-details">Reached 24k likes</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="drop-all"><a href="#" class="text-center">All Tasks</a></li>
                        </ul>
                    </li>
                    -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic"
                           data-toggle="dropdown">
                            {{--<span class="user-name">{{ $user_name }}<i class="fa fa-angle-down"></i></span>--}}
                            <span class="user-name">{{ $user_name }}<i class="fa fa-angle-down"></i></span>
                            {{--<img class="img-circle avatar" src="{{ $user_photo }}" width="40" height="40" alt="">--}}
                            <img class="img-circle avatar" src="{{ $user_photo }}" width="40" height="40" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-list" role="menu">
                            <li role="presentation"><a href="{{ url('/profile') }}"><i class="fa fa-user"></i>Perfil</a>
                            </li>
                            <!--
                            <li role="presentation"><a href="{{ url('/messages') }}"><i class="fa fa-envelope"></i>Mensagens</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a href="{{ url('/lock') }}"><i class="fa fa-lock"></i>Bloquear Tela</a>
                            </li>
                            -->
                            <li role="presentation"><a href="{{ url('/logout') }}"><i class="fa fa-sign-out m-r-xs"></i>Log
                                    out</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}" class="log-out waves-effect waves-button waves-classic">
                            <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                        </a>
                    </li>
                </ul>
                <!-- Nav -->
            </div>
            <!-- Top Menu -->
        </div>
    </div>
</div><!-- Navbar -->