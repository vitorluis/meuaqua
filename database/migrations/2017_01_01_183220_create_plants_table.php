<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('plants', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('plant_type_id')->unsigned();
            $table->integer('amount');
            $table->bigInteger('added_at');
            $table->integer('aquarium_id');
            $table->integer('user_id');

            /** Create the foreign keys */
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("aquarium_id")->references("id")->on("aquariums");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('plants');
    }
}
