<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAquariumPhotos extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("aquarium_photos", function (Blueprint $table) {
            $table->increments("id");
            $table->string("path", 200);
            $table->bigInteger("added_date");
            $table->integer("user_id")->unsigned();
            $table->integer("aquarium_id")->unsigned();

            /** Create the foreign keys */
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("aquarium_id")->references("id")->on("aquariums");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('aquarium_photos');
    }
}
