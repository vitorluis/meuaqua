<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAquariumTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('aquariums', function (Blueprint $table) {
            $table->increments("id");
            $table->string("name", 50);
            $table->longText('description')->nullable();
            $table->integer("liters");
            $table->integer("width");
            $table->integer("length");
            $table->integer("height");
            $table->boolean("freshwater");
            $table->integer("user_id")->unsigned();
            $table->string("recent_photo")->nullable();

            /** Now, the foreign keys */
            $table->foreign("user_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('aquariums');
    }
}
