<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKhTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("kh", function (Blueprint $table) {
            $table->increments("id");
            $table->double("level", 3, 2);
            $table->bigInteger("measure_date");
            $table->integer("user_id")->unsigned();
            $table->integer("aquarium_id")->unsigned();

            /** Create the foreign keys */
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("aquarium_id")->references("id")->on("aquariums");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('kh');
    }
}
