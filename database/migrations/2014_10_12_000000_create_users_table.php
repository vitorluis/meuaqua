<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('city');
            $table->string('state');
            $table->string('country')->default('BRA');
            $table->string('phone', 15);
            $table->string('photo')->nullable();
            $table->boolean('active')->default(false);
            $table->string('activation_token', 100)->nullable();
            $table->string('api_token', 100)->nullable();
            $table->boolean('premium')->default(false);
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }
}
