<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFishTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fish_types', function (Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->string("scientific_name")->nullable();
            $table->string("photo")->nullable();
            $table->boolean("freshwater");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('fish_types');
    }
}
