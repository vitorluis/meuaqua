<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpaTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("tpas", function (Blueprint $table) {
            $table->increments("id");
            $table->bigInteger("date");
            $table->integer("percentage");
            $table->boolean("warn_user")->default(false);
            $table->boolean("done")->default(false);
            $table->integer("user_id")->unsigned();
            $table->integer("aquarium_id")->unsigned();

            /** Create the foreign keys */
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("aquarium_id")->references("id")->on("aquariums");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tpas');
    }
}
