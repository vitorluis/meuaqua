<?php

use App\Models\FishType;
use Illuminate\Database\Seeder;

class FishTypesSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        /** Create all freshwater types */
        $file = fopen(__DIR__ . '/freshwater_fishes.csv', 'r');
        while ($data = fgetcsv($file, 3000, ';')) {
            FishType::create([
                'name' => $data[0],
                'scientific_name' => $data[1],
                'photo' => $data[2],
                'freshwater' => true,
            ]);
        }
        fclose($file);

        /** Now import saltwater fishes */
        $file = fopen(__DIR__ . '/saltwater_fishes.csv', 'r');
        while ($data = fgetcsv($file, 3000, ';')) {
            FishType::create([
                'name' => $data[0],
                'scientific_name' => $data[1],
                'photo' => $data[2],
                'freshwater' => false,
            ]);
        }
        fclose($file);
    }
}
