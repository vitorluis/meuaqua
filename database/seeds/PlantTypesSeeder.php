<?php

use App\Models\PlantType;
use Illuminate\Database\Seeder;

class PlantTypesSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        /** Now import saltwater fishes */
        $file = fopen(__DIR__ . '/plants.csv', 'r');
        while ($data = fgetcsv($file, 3000, ';')) {
            PlantType::create([
                'name' => $data[0],
                'scientific_name' => $data[1],
                'photo' => $data[2],
            ]);
        }
        fclose($file);
    }
}
