$(document).ready(function () {
    window.themeView = new ThemeView();
    window.searchView = new SearchView();

    if (typeof(MapView) !== "undefined") {
        window.mapView = new MapView();
    }
});