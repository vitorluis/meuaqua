<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@dashboard');

/** User Routes */
Route::get('/login', 'UserLoginController@showLogin');
Route::post('/login', 'UserLoginController@login');
Route::get('/logout', 'UserLoginController@logout');
Route::get('/register', 'UserRegistrationController@showRegister');
Route::post('/register', 'UserRegistrationController@registerUser');
Route::get('/activation/{token}', 'UserRegistrationController@activeUser');
Route::get('/profile', 'UserController@showProfile');
Route::post('/profile', 'UserController@saveProfile');
Route::get('/forgot', 'UserLoginController@showForgotPassword');
Route::post('/forgot', 'UserLoginController@forgot');
Route::get('/reset/{token}', 'UserLoginController@showReset');
Route::post('/reset', 'UserLoginController@reset');

/** Aquarium routes */
Route::post('/aquarium/new', 'AquariumController@saveAquarium');
Route::post('/aquarium/edit', 'AquariumController@saveAquarium');
Route::get('/aquarium/{id}', 'AquariumController@showAquarium');
Route::get('/aquarium/get/{id}', 'AquariumController@getAquarium');
Route::get('/aquarium/photos/{id}', 'AquariumController@showAquariumPhotos');
Route::post('/aquarium/photo/{id}', 'AquariumController@sendPhoto');
Route::delete('/aquarium/photo/delete/{id}', 'AquariumController@deletePhoto');

/** Fishes Routes */
Route::get('/fishes/{id}', 'FishController@showFishes');
Route::post('/fishes/save', 'FishController@saveFishes');
Route::post('/fishes/edit', 'FishController@editFishes');
Route::delete('/fishes/delete/{id}', 'FishController@deleteFishes');

/** Plants Routes */
Route::get('/plants/{id}', 'PlantController@showPlants');
Route::post('/plants/save', 'PlantController@savePlants');
Route::post('/plants/edit', 'PlantController@editPlants');
Route::delete('/plants/delete/{id}', 'PlantController@deletePlants');

/** PH Routes */
Route::get('/ph/{id}', 'PHController@showPHLevels');
Route::post('/ph/save', 'PHController@savePh');
Route::post('/ph/edit', 'PHController@editPh');
Route::delete('/ph/delete/{id}', 'PHController@deletePh');

/** Temperature Routes */
Route::get('/temperature/{id}', 'TemperatureController@showTemperatureDegrees');
Route::post('/temperature/save', 'TemperatureController@saveTemperature');
Route::post('/temperature/edit', 'TemperatureController@editTemperature');
Route::delete('/temperature/delete/{id}', 'TemperatureController@deleteTemperature');

/** Ammonia Routes */
Route::get('/ammonia/{id}', 'AmmoniaController@showAmmoniaLevels');
Route::post('/ammonia/save', 'AmmoniaController@saveAmmonia');
Route::post('/ammonia/edit', 'AmmoniaController@editAmmonia');
Route::delete('/ammonia/delete/{id}', 'AmmoniaController@deleteAmmonia');

/** Nitrite Routes */
Route::get('/nitrite/{id}', 'NitriteController@showNitriteLevels');
Route::post('/nitrite/save', 'NitriteController@saveNitrite');
Route::post('/nitrite/edit', 'NitriteController@editNitrite');
Route::delete('/nitrite/delete/{id}', 'NitriteController@deleteNitrite');

/** TPA Routes */
Route::get('/tpa/{id}', 'TPAController@showTPA');
Route::post('/tpa/save', 'TPAController@saveTPA');
Route::post('/tpa/edit', 'TPAController@editTPA');
Route::delete('/tpa/delete/{id}', 'TPAController@deleteTPA');

/** GH Routes */
Route::get('/gh/{id}', 'GHController@showGHLevels');
Route::post('/gh/save', 'GHController@saveGh');
Route::post('/gh/edit', 'GHController@editGh');
Route::delete('/gh/delete/{id}', 'GHController@deleteGh');

/** KH Routes */
Route::get('/kh/{id}', 'KHController@showKHLevels');
Route::post('/kh/save', 'KHController@saveKh');
Route::post('/kh/edit', 'KHController@editKh');
Route::delete('/kh/delete/{id}', 'KHController@deleteKh');
