var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var pump = require('pump');
var watch = require('gulp-watch');
var sass = require('gulp-sass');

gulp.task('sass', function () {
    return gulp.src('resources/assets/sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('custom.css'))
        .pipe(gulp.dest('public/assets/css/'));
});

gulp.task('compress', function (cb) {
    pump([
            gulp.src('resources/assets/js/**/*.js'),
            uglify(),
            concat('public/assets/js/app.min.js'),
            gulp.dest('.')
        ],
        cb
    );
});

gulp.task('watch', function () {
    gulp.watch('resources/assets/**/*.*', ['compress', 'sass']);
});

