<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class KH extends Model {
    /** Set table name */
    protected $table = 'kh';

    /** Disable timestamps */
    public $timestamps = false;

    /** Accessor to format the data
     * @param $value
     * @return string
     */
    public function getMeasureDateAttribute($value) {
        return Carbon::createFromTimestamp($value)->format('d/m/Y H:i:s');
    }
}
