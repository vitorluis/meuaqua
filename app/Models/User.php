<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
    use Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Create a queryScope to get user by activation code
     * @param $query
     * @param $token
     */
    public function scopeByActivationToken($query, $token) {
        return $query->where('activation_token', $token)->get()->first();
    }

    /** Create a queryScope to get user by API Token
     * @param $query
     * @param $token
     * @return
     */
    public function scopeByAPIToken($query, $token) {
        return $query->where('api_token', $token)->get()->first();
    }

    /**
     * Create a queryScope to get user by reset password code
     * @param $query
     * @param $token
     */
    public function scopeByResetPasswordToken($query, $token) {
        return $query->where('reset_password_token', $token)->get()->first();
    }

    /** Get user aquariums */
    public function aquariums() {
        return $this->hasMany('App\Models\Aquarium', 'user_id', 'id')->orderBy('id');
    }
}
