<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlantType extends Model {
    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'plant_types';

    /** Fillable fields from mass assignments */
    protected $fillable = [
        'name',
        'scientific_name',
        'photo'
    ];
}
