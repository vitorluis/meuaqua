<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Temperature extends Model {
    protected $table = 'temperature';

    public $timestamps = false;

    /** Function to get the temperature by aquarium
     * @param $query
     * @param $aquarium_id
     * @param $user_id
     * @return
     */
    public function scopeByAquarium($query, $aquarium_id, $user_id) {
        return $query->where('aquarium_id', $aquarium_id)->where('user_id', $user_id)->get();
    }

    /** Accessor to format the data
     * @param $value
     * @return string
     */
    public function getMeasureDateAttribute($value) {
        return Carbon::createFromTimestamp($value)->format('d/m/Y H:i:s');
    }
}
