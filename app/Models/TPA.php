<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TPA extends Model {
    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'tpas';

    /** Accessor to format the data
     * @param $value
     * @return string
     */
    public function getDateAttribute($value) {
        return Carbon::createFromTimestamp($value)->format('d/m/Y');
    }

    /** Set relationship with user */
    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
