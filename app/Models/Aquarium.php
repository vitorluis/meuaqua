<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aquarium extends Model {
    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'aquariums';

    /** Set relationship with fishes */
    public function fishes() {
        return $this->hasMany('App\Models\Fish', 'aquarium_id', 'id')->orderBy('id');
    }

    /** Set relationship with plants */
    public function plants() {
        return $this->hasMany('App\Models\Plant', 'aquarium_id', 'id')->orderBy('id');
    }

    /** Set relationship for ph */
    public function ph() {
        return $this->hasMany('App\Models\PH', 'aquarium_id', 'id')->orderBy('measure_date', 'desc');
    }

    /** Set relationship with Temperature */
    public function temperature() {
        return $this->hasMany('App\Models\Temperature', 'aquarium_id', 'id')->orderBy('measure_date', 'desc');
    }

    /** Set relationship with Ammonia */
    public function ammonia() {
        return $this->hasMany('App\Models\Ammonia', 'aquarium_id', 'id')->orderBy('measure_date', 'desc');
    }

    /** Set relationship with Nitrite */
    public function nitrite() {
        return $this->hasMany('App\Models\Nitrite', 'aquarium_id', 'id')->orderBy('measure_date', 'desc');
    }

    /** Set relationship with TPA */
    public function tpa() {
        return $this->hasMany('App\Models\TPA', 'aquarium_id', 'id')->orderBy('date', 'desc');
    }

    /** Set relationship for gh */
    public function gh() {
        return $this->hasMany('App\Models\GH', 'aquarium_id', 'id')->orderBy('measure_date', 'desc');
    }

    /** Set relationship for kh */
    public function kh() {
        return $this->hasMany('App\Models\KH', 'aquarium_id', 'id')->orderBy('measure_date', 'desc');
    }

    /** Set relationship for ph */
    public function photos() {
        return $this->hasMany('App\Models\AquariumPhoto', 'aquarium_id', 'id')->orderBy('added_date', 'desc');
    }
}
