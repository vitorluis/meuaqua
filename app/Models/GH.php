<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class GH extends Model {

    protected $table = 'gh';

    public $timestamps = false;

    /** Accessor to format the data
     * @param $value
     * @return string
     */
    public function getMeasureDateAttribute($value) {
        return Carbon::createFromTimestamp($value)->format('d/m/Y H:i:s');
    }
}
