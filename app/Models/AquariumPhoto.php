<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AquariumPhoto extends Model {
    /** Set data table */
    protected $table = 'aquarium_photos';

    /** Disable timestamps */
    public $timestamps = false;
}
