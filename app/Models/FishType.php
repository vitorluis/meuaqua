<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FishType extends Model {

    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'fish_types';

    protected $fillable = [
        'name', 'scientific_name', 'photo'
    ];
}
