<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Nitrite extends Model {
    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'nitrite';

    /** Accessor to format the data
     * @param $value
     * @return string
     */
    public function getMeasureDateAttribute($value) {
        return Carbon::createFromTimestamp($value)->format('d/m/Y H:i:s');
    }
}
