<?php
/**
 * Created by PhpStorm.
 * User: vitor
 * Date: 08/01/17
 * Time: 15:18
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Plant extends Model {
    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'plants';

    /** relationship with plant types */
    public function type() {
        return $this->hasOne('App\Models\PlantType', 'id', 'plant_type_id');
    }
}