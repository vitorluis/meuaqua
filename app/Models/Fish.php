<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fish extends Model {
    /** Disable timestamps */
    public $timestamps = false;

    /** Set table name */
    protected $table = 'fishes';

    /** relationship with fish types */
    public function type() {
        return $this->hasOne('App\Models\FishType', 'id', 'fish_type_id');
    }
}
