<?php

namespace App\Http\ViewComposers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ProfileComposer {
    /**
     * The user repository implementation.
     *
     * @var User
     */
    protected $user;

    /**
     * Create a new profile composer.
     *
     * @internal param UserRepository $users
     */
    public function __construct() {
        /** Get the logged user */
        $this->user = Auth::user();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view) {
        /** Get user photo */
        $photo = null;
        if ($this->user->photo == null) {
            $photo = '/assets/images/no-avatar.png';
        } else {
            $photo = $this->user->photo;
        }

        $view->with([
            'user_photo' => $photo,
            'user_name' => $this->user->name,
        ]);
    }
}