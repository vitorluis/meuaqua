<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AquariumRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'liters' => 'required|integer',
            'width' => 'required|integer',
            'length' => 'required|integer',
            'height' => 'required|integer',
            'water_type' => 'required',
            'photo' => 'image'
        ];
    }

    /** Function to return messages */
    public function messages() {
        return [
            'name.required' => 'O nome é obrigatório.',
            'liters.required' => 'Os litros são obrigatórios.',
            'liters.integer' => 'Os litros devem ser numérico.',
            'width.required' => 'A largura é obrigatória.',
            'width.integer' => 'A largura deve ser numérico.',
            'length.required' => 'O comprimento é obrigatório.',
            'length.integer' => 'O comprimento deve ser numérico.',
            'height.required' => 'A altura é obrigatória.',
            'height.integer' => 'A altura deve ser numérico.',
            'water_type.required' => 'O tipo de água é obrigatório.',
            'photo.image' => 'A foto deve ser uma imagem'
        ];
    }
}
