<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TPARequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'aquarium' => 'filled',
            'date' => 'required|date_format:d/m/Y',
            'percentage' => 'required|numeric'
        ];
    }

    /**
     * FUnction to set custom message errors
     */
    public function messages() {
        return [
            'aquarium.filled' => 'O ID do aquário é obrigatório',
            'percentage.required' => 'A porcentagem é obrigatória',
            'percentage.numeric' => 'A porcentagem deve ser númerica',
            'date.required' => 'A data é obrigatória',
            'date.date_format' => 'A data está no formato incorreto',
        ];
    }
}
