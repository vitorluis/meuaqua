<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlantRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'plant_id' => 'filled|exists:plants,id',
            'plant_type' => 'filled|exists:plant_types,id',
            'quantity' => 'required|integer',
        ];
    }

    /** Function to return custom message validations */
    public function messages() {
        return [
            'plant_id.filled' => 'O ID da planta é obrigatória.',
            'plant_id.exists' => 'A planta selecionada não é válida.',
            'plant_type.filled' => 'O tipo da planta é obrigatória.',
            'plant_type.exists' => 'O tipo da planta selecionada não é válida.',
            'quantity.required' => 'A quantidade é obrigatória.',
            'quantity.integer' => 'A quantidade deve ser numérica.',
        ];
    }
}
