<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'phone' => 'required|numeric',
            'photo' => 'image'
        ];
    }

    /**
     * Define the user messages
     */
    public function messages() {
        return [
            'name.required' => 'O nome é obrigatório.',
            'city.required' => 'A Cidade é obrigatória.',
            'state.required' => 'O Estado é obrigat&oacute;rio.',
            'phone.required' => 'O Telefone é obrigatório.',
            'phone.numeric' => 'O Telefone deve conter somente números.',
            'photo.image' => 'A foto de perfil deve ser uma imagem',
        ];
    }
}