<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemperatureRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'aquarium' => 'filled',
            'degrees' => 'required|numeric'
        ];
    }
    public function messages() {
        return [
            'aquarium.filled' => 'O ID do aquário é obrigatório',
            'degrees.required' => 'A temperatura é obrigatória',
            'degrees.numeric' => 'A temperatura deve ser númerica',
        ];
    }
}
