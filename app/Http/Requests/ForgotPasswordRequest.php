<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => 'required|email',
            'phone' => 'required|integer',
        ];
    }

    /**
     * Function to set custom message errors
     */
    public function messages() {
        return [
            'email.required' => 'O E-mail é obrigatório',
            'email.email' => 'O E-mail não está no formato correto',
            'phone.required' => 'O Telefone é obrigatório',
            'phone.integer' => 'O Telefone deve ser um número',
        ];
    }
}
