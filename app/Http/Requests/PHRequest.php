<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PHRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'aquarium' => 'filled',
            'level' => 'required|numeric'
        ];
    }

    /**
     * FUnction to set custom message errors
     */
    public function messages() {
        return [
            'aquarium.filled' => 'O ID do aquário é obrigatório',
            'level.required' => 'O Nivel do PH é obrigatório',
            'level.numeric' => 'O Nível deve ser númerico',
        ];
    }
}
