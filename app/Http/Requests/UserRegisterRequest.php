<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'city' => 'required',
            'state' => 'required',
            'phone' => 'required|numeric',
            'accept_terms' => 'required',
        ];
    }

    /**
     * Define the user messages
     */
    public function messages() {
        return [
            'name.required' => 'O nome &eacute; obrigat&oacute;rio.',
            'email.required' => 'O E-mail &eacute; obrigat&oacute;rio.',
            'email.unique' => 'E-mail j&aacute; cadastrado.',
            'password.required' => 'A senha &eacute; obrigat&oacute;ria.',
            'password.confirmed' => 'As senhas n&atilde;o conferem.',
            'city.required' => 'A Cidade &eacute; obrigat&oacute;ria.',
            'state.required' => 'O Estado &eacute; obrigat&oacute;rio.',
            'phone.required' => 'O Telefone &eacute; obrigat&oacute;rio.',
            'phone.numeric' => 'O Telefone deve conter somente n&uacu*te;meros.',
            'accept_terms.required' => 'Voce deve aceitar os termos',
        ];
    }
}