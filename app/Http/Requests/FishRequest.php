<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FishRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'fish_id' => 'filled|exists:fishes,id',
            'fish_type' => 'filled|exists:fish_types,id',
            'quantity' => 'required|integer',
        ];
    }

    /** Function to return custom message validations */
    public function messages() {
        return [
            'fish_id.filled' => 'O ID do peixe é obrigatório.',
            'fish_id.exists' => 'O peixe selecionado não é válido.',
            'fish_type.filled' => 'O tipo do peixe é obrigatório.',
            'fish_type.exists' => 'O tipo do peixe selecionado não é válido.',
            'quantity.required' => 'A quantidade é obrigatória.',
            'quantity.integer' => 'A quantidade deve ser numérica.',
        ];
    }
}
