<?php

namespace App\Http\Controllers;

use App\Http\Requests\TemperatureRequest;
use App\Models\Aquarium;
use App\Models\Temperature;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TemperatureController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the Temperature degreess of the aquarium and user
     * @param $id
     * @return View
     */
    public function showTemperatureDegrees($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Render the view */
        return view('temperature.index')->with([
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new temperature
     * @param TemperatureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTemperature(TemperatureRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new temperature */
        $temperature = new Temperature();
        $temperature->degrees = $request->get('degrees');
        $temperature->aquarium_id = $request->get('aquarium');
        $temperature->measure_date = Carbon::now()->timestamp;
        $temperature->user_id = $user->id;

        try {
            $temperature->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit temperature
     * @param TemperatureRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editTemperature(TemperatureRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the temperature */
        $temperature = Temperature::where('id', $request->get('temperature_id'))->where('user_id', $user->id)->first();
        $temperature->degrees = $request->get('degrees');

        try {
            $temperature->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete temperaturees
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTemperature($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the temperature */
        $temperature = Temperature::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $temperature->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
