<?php

namespace App\Http\Controllers;

use App\Http\Requests\PHRequest;
use App\Models\Aquarium;
use App\Models\PH;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class PHController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the PH levels of the aquarium and user
     * @param $id
     * @return View
     */
    public function showPHLevels($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Render the view */
        return view('ph.index')->with([
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new ph
     * @param PHRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function savePh(PHRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new ph */
        $ph = new PH();
        $ph->level = $request->get('level');
        $ph->aquarium_id = $request->get('aquarium');
        $ph->measure_date = Carbon::now()->timestamp;
        $ph->user_id = $user->id;

        try {
            $ph->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit ph
     * @param PHRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPh(PHRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the ph */
        $ph = PH::where('id', $request->get('ph_id'))->where('user_id', $user->id)->first();
        $ph->level = $request->get('level');

        try {
            $ph->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete phes
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePh($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the ph */
        $ph = PH::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $ph->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
