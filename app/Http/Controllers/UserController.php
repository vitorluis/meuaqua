<?php

namespace App\Http\Controllers;


use App\Facades\GoogleMaps;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\ServiceType;
use App\Models\WorkerServiceTypes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class UserController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function showProfile() {
        /** Load the current user */
        $user = Auth::user();

        /** Return the view with user data */
        return view('profile')->with([
            'user' => $user,
        ]);
    }

    public function saveProfile(UpdateProfileRequest $request) {
        /** Load user data */
        $user = Auth::user();

        /** First of all, handle the photo, if have one */
        $profile_photo = $request->file('photo');

        /** Check if photo is valid */
        $photo_filename = null;
        if ($profile_photo != null && $profile_photo->isValid()) {
            /** Generate a filename */
            $date = Carbon::now();
            $photo_filename = $profile_photo->getFilename() . $date->timestamp . "." . $profile_photo->getClientOriginalExtension();

            /** Resize image */
            Image::make($profile_photo)->resize(640, 480)->save(storage_path() . '/' . $photo_filename, 60);

            /** Save the photo on S3 Storage */
            Storage::put($photo_filename, file_get_contents(storage_path() . '/' . $photo_filename));

            /** Delete local file */
            Storage::disk('local')->delete($photo_filename);

            /** Set on user model */
            $photo = Storage::url($photo_filename);
            $photo = url($photo);
            $user->photo = $photo;
        }

        /** Set user data */
        $user->name = $request->get('name');
        $user->city = $request->get('city');
        $user->state = $request->get('state');
        $user->phone = $request->get('phone');

        /** Save the data */
        $user->save();

        /** Return success */
        return redirect('/profile')->with('status', 'Perfil atualizado com sucesso');

    }
}
