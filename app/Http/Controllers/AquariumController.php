<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddAquariumRequest;
use App\Http\Requests\AquariumRequest;
use App\Models\Aquarium;
use App\Models\AquariumPhoto;
use App\Models\TPA;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AquariumController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function saveAquarium(AquariumRequest $request, ImageOptimizer $imageOptimizer) {
        /** Get current user */
        $user = Auth::user();

        /** Create new Aquarium */
        if ($request->has('aquarium')) {
            $aquarium = Aquarium::where('user_id', $user->id)->where('id', $request->get('aquarium'))->first();
        } else {
            $aquarium = new Aquarium();
        }
        $aquarium->name = $request->get('name');
        $aquarium->width = $request->get('width');
        $aquarium->length = $request->get('length');
        $aquarium->height = $request->get('height');
        $aquarium->liters = $request->get('liters');
        $aquarium->description = $request->get('description');
        $aquarium->freshwater = $request->get('water_type') == 'doce' ? true : false;
        $aquarium->user_id = $user->id;

        /** First of all, handle the photo, if have one */
        $photo = $request->file('photo');

        /** Check if photo is valid */
        $photo_filename = null;
        if ($photo != null && $photo->isValid()) {
            /** Generate a filename */
            $date = Carbon::now();
            $photo_filename = $photo->getFilename() . $date->timestamp . "." . $photo->getClientOriginalExtension();

            /** Resize image */
            Image::make($photo)->resize(640, 480)->save(storage_path() . '/' . $photo_filename, 60);

            /** Save the photo on S3 Storage */
            Storage::put($photo_filename, file_get_contents(storage_path() . '/' . $photo_filename));

            /** Delete local file */
            File::delete(storage_path() . '/' . $photo_filename);

            /** Set on user model */
            $photo = Storage::url($photo_filename);
            $photo = url($photo);
            $aquarium->recent_photo = $photo;
        }

        try {
            $aquarium->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    public function showAquarium($id) {
        /** Get the user */
        $user = Auth::user();

        /** Get the aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Get fish count */
        $fishes = array_map(function ($fish) {
            return $fish['amount'];
        }, $aquarium->fishes->toArray());

        /** Get plant count */
        $plants = array_map(function ($plant) {
            return $plant['amount'];
        }, $aquarium->plants->toArray());

        /** Get Next and Last TPA */
        $today = Carbon::today()->timestamp;
        $last_tpa = TPA::where('user_id', $user->id)
            ->where('aquarium_id', $id)->where('date', '<', $today)
            ->orderBy('date', 'desc')->first();
        $last_tpa_days = $last_tpa != null ? Carbon::today()->diffInDays(Carbon::createFromFormat('d/m/Y', $last_tpa->date)) : 0;

        $next_tpa = TPA::where('user_id', $user->id)
            ->where('aquarium_id', $id)->where('date', '>=', $today)
            ->orderBy('date', 'asc')->first();
        $next_tpa_days = $next_tpa != null ? Carbon::today()->diffInDays(Carbon::createFromFormat('d/m/Y', $next_tpa->date)) : 0;

        /** Render the view */
        return view('aquarium.overview')->with([
            'aquarium' => $aquarium,
            'fishes' => array_sum($fishes),
            'plants' => array_sum($plants),
            'last_tpa' => $last_tpa_days,
            'next_tpa' => $next_tpa_days,
        ]);
    }

    public function getAquarium($id) {
        /** Get the user */
        $user = Auth::user();

        /** Get the aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();

        /** return the Json */
        return $aquarium->toJson();
    }

    public function showAquariumPhotos($id) {
        /** Get the user */
        $user = Auth::user();

        /** Get the aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();

        return view('aquarium.photos', [
            'aquarium' => $aquarium
        ]);
    }

    public function sendPhoto(Request $request, $id) {
        /** Get the user */
        $user = Auth::user();

        /** Get the aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }
        /** First of all, handle the photo, if have one */
        $photo = $request->file('photo');

        if ($photo != null && $photo->isValid()) {
            $date = Carbon::now();
            $photo_filename = $photo->getFilename() . sha1($date->timestamp) . "." . $photo->getClientOriginalExtension();
            /** Resize image */
            Image::make($photo)->save(storage_path() . '/' . $photo_filename, 40);

            /** Save the photo on S3 Storage */
            Storage::put($photo_filename, file_get_contents(storage_path() . '/' . $photo_filename));

            /** Delete local file */
            File::delete(storage_path() . '/' . $photo_filename);

            /** Create new Aquarium Photo */
            $photo = new AquariumPhoto();
            $photo->user_id = $user->id;
            $photo->aquarium_id = $aquarium->id;
            $photo->added_date = $date->timestamp;
            $photo->path = url(Storage::url($photo_filename));

            try {
                $photo->save();
                return response()->json(['response' => true]);
            } catch (Exception $e) {
                return response()->json(['response' => false], 500);
            }
        } else {
            return abort(400);
        }
    }

    public function deletePhoto($id) {
        /** Get the user */
        $user = Auth::user();

        /** Load the Photo */
        $photo = AquariumPhoto::where('id', $id)->where('user_id', $user->id)->first();
        if ($photo == null) {
            return abort(404);
        }

        /** Get the photo name */
        $filename = basename($photo->path);

        /** Remove from the storage */
        $deleted = Storage::delete($filename) == false ?: $photo->delete();
        return response()->json(['response' => $deleted]);
    }
}
