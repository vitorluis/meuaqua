<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlantRequest;
use App\Models\Aquarium;
use App\Models\Plant;
use App\Models\PlantType;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

class PlantController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function showPlants($id) {
        /** Get auth user */
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Load the plant types according aquarium water type */
        $plants_types = PlantType::orderBy('name', 'asc')->get();

        /** Render the view */
        return view('plants.index')->with([
            'plants_types' => $plants_types,
            'aquarium' => $aquarium
        ]);
    }

    public function savePlants(PlantRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new plant */
        $plant = new Plant();
        $plant->plant_type_id = $request->get('plant_type');
        $plant->amount = $request->get('quantity');
        $plant->aquarium_id = $request->get('aquarium');
        $plant->added_at = Carbon::now()->timestamp;
        $plant->user_id = $user->id;

        try {
            $plant->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    public function editPlants(PlantRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the fish */
        $fish = Plant::where('id', $request->get('plant_id'))->where('user_id', $user->id)->first();
        $fish->amount = $request->get('quantity');

        try {
            $fish->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete plants
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePlants($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the fish */
        $fish = Plant::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $fish->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
