<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserLoginRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserLoginController extends Controller {

    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLogin() {
        return view('login');
    }

    public function login(UserLoginRequest $request) {
        /** Get email and password */
        $email = $request->get('email');
        $password = $request->get('password');

        /** Attempt the user */
        if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => true])) {
            /** If enter here, user is logged in, redirect to home */
            return redirect('/');
        }

        /** If get here, auth failed */
        return redirect()->back()->with('error_login', 'E-mail e/ou senhas não conferem.');
    }

    /** Function to logout user */
    public function logout() {
        /** Logout user */
        Auth::logout();

        /** Redirect */
        return redirect('/login')->with('status', 'Você foi deslogado. :)');
    }

    /** Function to show forgot password */
    public function showForgotPassword() {
        return view('forgot');
    }

    /** Function to handle e-mail and send link with reset password
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function forgot(ForgotPasswordRequest $request) {
        /** Load the user */
        $user = User::where('email', $request->get('email'))->where('phone', $request->get('phone'))->first();
        if ($user == null) {
            return redirect()->back()->with('error_forgot', 'Usuário não encontrado');
        }

        /** Set the reset code */
        $user->reset_password_token = sha1(Carbon::now()->timestamp . $user->email . random_int(1, 10000));
        $user->save();

        /** Queue the confirmation mail to user */
        $link_reset = sprintf('/reset/%s', $user->reset_password_token);
        Mail::queue('emails.reset_password', ['name' => $user->name, 'link' => url($link_reset)], function ($message) use ($user) {
            $message->to($user->email);
            $message->subject('Troque sua senha');
        });

        /** Go to login with message */
        return redirect('/login')->with('status', 'E-mail de recuperação de senha enviado, verifique seu e-mail.');
    }

    /** Function load user to reset password and show the view
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showReset($token) {
        /** Load user by his token */
        $user = User::byResetPasswordToken($token)->first();

        /** Check if user exists */
        if ($user == null) {
            /** Return to login with error */
            return redirect('/login')->with('error_login', 'Usuário não encontrado. :(');
        }

        /** If found it, render the view with user data */
        return view('reset')->with('user', $user);
    }

    /** Function to reset password it self
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(ResetPasswordRequest $request) {
        /** Try to load user by token and ID */
        $user = User::where('id', $request->get('user_id'))->where('reset_password_token', $request->get('token'))->first();

        /** Check if was loaded */
        if ($user == null) {
            return redirect()->back()->with('error_reset', 'Usuário não encontrado. :(');
        }

        /** Set the new password */
        $user->password = bcrypt($request->get('password'));
        $user->save();

        /** Redirect to login */
        return redirect('/login')->with('status', 'Senha trocada com sucesso. :)');
    }

}
