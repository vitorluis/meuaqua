<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function dashboard() {
        /** Get logged user */
        $user = Auth::user();

        /** Render the view */
        return view('index')->with([
            'aquariums' => $user->aquariums,
            'user' => $user,
        ]);
    }
}
