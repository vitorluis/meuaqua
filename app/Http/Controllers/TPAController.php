<?php

namespace App\Http\Controllers;

use App\Http\Requests\TPARequest;
use App\Models\Aquarium;
use App\Models\TPA;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TPAController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the TPA levels of the aquarium and user
     * @param $id
     * @return View
     */
    public function showTPA($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Get last TPA and the next ones */
        $now = Carbon::today()->timestamp;

        $last_tpa = TPA::where('user_id', $user->id)
            ->where('aquarium_id', $id)->where('date', '<', $now)
            ->orderBy('date', 'desc')->get();

        $next_tpa = TPA::where('user_id', $user->id)
            ->where('aquarium_id', $id)->where('date', '>=', $now)
            ->orderBy('date', 'asc')->get();

        /** Check if they are null, if is null is not from this user */
        if ($last_tpa == null || $next_tpa == null) {
            return abort(404);
        }

        /** Render the view */
        return view('tpa.index')->with([
            'aquarium' => $aquarium,
            'last_tpa' => $last_tpa,
            'next_tpa' => $next_tpa,
        ]);
    }

    /** Function to save a new tpa
     * @param TPARequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTPA(TPARequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new tpa */
        $tpa = new TPA();
        $tpa->date = Carbon::createFromFormat('d/m/Y', $request->get('date'))->timestamp;
        $tpa->percentage = $request->get('percentage');
        $tpa->aquarium_id = $request->get('aquarium');
        $tpa->warn_user = true;
        $tpa->user_id = $user->id;

        try {
            $tpa->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit tpa
     * @param TPARequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editTPA(TPARequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the tpa */
        $tpa = TPA::where('id', $request->get('tpa_id'))->where('user_id', $user->id)->first();
        $tpa->date = Carbon::createFromFormat('d/m/Y', $request->get('date'))->timestamp;
        $tpa->percentage = $request->get('percentage');

        try {
            $tpa->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete tpa
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTPA($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the tpa */
        $tpa = TPA::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $tpa->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
