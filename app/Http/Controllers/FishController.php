<?php

namespace App\Http\Controllers;

use App\Http\Requests\FishRequest;
use App\Models\Aquarium;
use App\Models\Fish;
use App\Models\FishType;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

class FishController extends Controller {


    public function __construct() {
        $this->middleware('auth');
    }

    public function showFishes($id) {
        /** Get auth user */
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Load the fish types according aquarium water type */
        $fishes_types = FishType::where('freshwater', $aquarium->freshwater)->orderBy('name', 'asc')->get();

        /** Render the view */
        return view('fishes.index')->with([
            'fishes_types' => $fishes_types,
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new fish
     * @param FishRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveFishes(FishRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new fish */
        $fish = new Fish();
        $fish->fish_type_id = $request->get('fish_type');
        $fish->amount = $request->get('quantity');
        $fish->aquarium_id = $request->get('aquarium');
        $fish->added_at = Carbon::now()->timestamp;
        $fish->user_id = $user->id;

        try {
            $fish->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit fishes
     * @param FishRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editFishes(FishRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the fish */
        $fish = Fish::where('id', $request->get('fish_id'))->where('user_id', $user->id)->first();
        $fish->amount = $request->get('quantity');

        try {
            $fish->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete fishes
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFishes($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the fish */
        $fish = Fish::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $fish->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}