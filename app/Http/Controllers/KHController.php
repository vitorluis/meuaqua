<?php

namespace App\Http\Controllers;

use App\Http\Requests\KHRequest;
use App\Models\Aquarium;
use App\Models\KH;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class KHController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the KH levels of the aquarium and user
     * @param $id
     * @return View
     */
    public function showKHLevels($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Render the view */
        return view('kh.index')->with([
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new kh
     * @param KHRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveKh(KHRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new kh */
        $kh = new KH();
        $kh->level = $request->get('level');
        $kh->aquarium_id = $request->get('aquarium');
        $kh->measure_date = Carbon::now()->timestamp;
        $kh->user_id = $user->id;

        try {
            $kh->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit kh
     * @param KHRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editKh(KHRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the kh */
        $kh = KH::where('id', $request->get('kh_id'))->where('user_id', $user->id)->first();
        $kh->level = $request->get('level');

        try {
            $kh->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete khes
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteKh($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the kh */
        $kh = KH::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $kh->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
