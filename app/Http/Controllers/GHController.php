<?php

namespace App\Http\Controllers;

use App\Http\Requests\GHRequest;
use App\Models\Aquarium;
use App\Models\GH;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class GHController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the GH levels of the aquarium and user
     * @param $id
     * @return View
     */
    public function showGHLevels($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Render the view */
        return view('gh.index')->with([
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new gh
     * @param GHRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveGh(GHRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new gh */
        $gh = new GH();
        $gh->level = $request->get('level');
        $gh->aquarium_id = $request->get('aquarium');
        $gh->measure_date = Carbon::now()->timestamp;
        $gh->user_id = $user->id;

        try {
            $gh->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit gh
     * @param GHRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editGh(GHRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the gh */
        $gh = GH::where('id', $request->get('gh_id'))->where('user_id', $user->id)->first();
        $gh->level = $request->get('level');

        try {
            $gh->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete ghes
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGh($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the gh */
        $gh = GH::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $gh->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
