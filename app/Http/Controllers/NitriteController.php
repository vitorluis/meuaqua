<?php

namespace App\Http\Controllers;

use App\Http\Requests\NitriteRequest;
use App\Models\Aquarium;
use App\Models\Nitrite;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class NitriteController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the Nitrite levels of the aquarium and user
     * @param $id
     * @return View
     */
    public function showNitriteLevels($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Render the view */
        return view('nitrite.index')->with([
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new nitrite
     * @param NitriteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveNitrite(NitriteRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new nitrite */
        $nitrite = new Nitrite();
        $nitrite->level = $request->get('level');
        $nitrite->aquarium_id = $request->get('aquarium');
        $nitrite->measure_date = Carbon::now()->timestamp;
        $nitrite->user_id = $user->id;

        try {
            $nitrite->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit nitrite
     * @param NitriteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editNitrite(NitriteRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the nitrite */
        $nitrite = Nitrite::where('id', $request->get('nitrite_id'))->where('user_id', $user->id)->first();
        $nitrite->level = $request->get('level');

        try {
            $nitrite->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete nitrite
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteNitrite($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the nitrite */
        $nitrite = Nitrite::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $nitrite->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
