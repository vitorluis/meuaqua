<?php

namespace App\Http\Controllers;

use App\Facades\GoogleMaps;
use App\Facades\Solr;
use App\Http\Requests\UserRegisterRequest;
use App\Models\ServiceType;
use App\Models\User;
use App\Models\WorkerServiceTypes;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserRegistrationController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => [
            'showRegister', 'registerUser', 'activeUser'
        ]]);

        /** Add a middleware checking if user is already logged in */
        $this->middleware('guest');
    }

    /** Function to show the register page */
    public function showRegister() {
        /** Render the view with the types */
        return view('register');
    }

    /** Function to validate and save a new user
     * @param UserRegisterRequest|Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function registerUser(UserRegisterRequest $request) {
        /** Create here a new user, request here is already validated */
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->city = $request->get('city');
        $user->state = $request->get('state');
        $user->phone = $request->get('phone');

        /** Create a activation token, to user active on his email */
        $user->activation_token = md5($user->email . Carbon::now()->timestamp);
        $user->api_token = md5($user->name . Carbon::now()->timestamp);

        /** Save the user
         * When saved a observer will be dispatch to index this user on Solr
         */
        $user->save();

        /** Queue the confirmation mail to user */
        $link_activation = sprintf('/activation/%s', $user->activation_token);
        Mail::queue('emails.active_account', ['name' => $user->name, 'link' => url($link_activation)], function ($message) use ($user) {
            $message->to($user->email);
            $message->subject('Ative sua conta');
        });

        /** Redirect to login */
        return redirect('/login')->with('status', 'Registro efetuado com sucesso! Verifique seu email para confirmar o cadastro. :)');
    }

    /**
     * Function to active user
     */
    public function activeUser($token) {
        /** Load user by his token */
        $user = User::byActivationToken($token);

        /** Check if user exists */
        if ($user->first() == null) {
            /** Return to login with error */
            return redirect('/login')->with('error_login', 'Usu&aacute;rio n&atilde;o encontrado. :(');
        }

        /** Set as active */
        $user->active = true;

        /** Remove the activation token */
        $user->activation_token = null;

        /** Save the user */
        $user->save();

        /** Return to login with the success message */
        return redirect('/login')->with('status', 'Usuário ativado com sucesso, por favor, faça o login. :)');
    }
}
