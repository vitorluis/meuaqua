<?php

namespace App\Http\Controllers;

use App\Http\Requests\AmmoniaRequest;
use App\Models\Ammonia;
use App\Models\Aquarium;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AmmoniaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /** Function to show the Ammonia levels of the aquarium and user
     * @param $id
     * @return View
     */
    public function showAmmoniaLevels($id) {
        $user = Auth::user();

        /** Get aquarium */
        $aquarium = Aquarium::where('user_id', $user->id)->where('id', $id)->first();
        if ($aquarium == null) {
            return abort(404);
        }

        /** Render the view */
        return view('ammonia.index')->with([
            'aquarium' => $aquarium
        ]);
    }

    /** Function to save a new ammonia
     * @param AmmoniaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveAmmonia(AmmoniaRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Create new ammonia */
        $ammonia = new Ammonia();
        $ammonia->level = $request->get('level');
        $ammonia->aquarium_id = $request->get('aquarium');
        $ammonia->measure_date = Carbon::now()->timestamp;
        $ammonia->user_id = $user->id;

        try {
            $ammonia->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Funciton to edit ammonia
     * @param AmmoniaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editAmmonia(AmmoniaRequest $request) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the ammonia */
        $ammonia = Ammonia::where('id', $request->get('ammonia_id'))->where('user_id', $user->id)->first();
        $ammonia->level = $request->get('level');

        try {
            $ammonia->save();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }

    /** Function to delete ammoniaes
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAmmonia($id) {
        /** Get logged in User */
        $user = Auth::user();

        /** Load the ammonia */
        $ammonia = Ammonia::where('id', $id)->where('user_id', $user->id)->first();
        try {
            $ammonia->delete();
            return response()->json(['response' => true]);
        } catch (Exception $e) {
            return response()->json(['response' => false], 500);
        }
    }
}
