<?php

namespace App\Console\Commands;

use App\Models\TPA;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Mailer;

class SendTPAEmails extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tpa:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send TPA Reminder emails';

    /** Mailer */
    protected $mailer;

    /**
     * Create a new command instance.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer) {
        parent::__construct();
        $this->mailer = $mailer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        /** Generate range timestamp */
        $tomorrow = Carbon::tomorrow()->timestamp;
        $after_tomorrow = Carbon::tomorrow()->addDay()->timestamp;

        /** Load all TPAs of Tomorrow */
        $tpas = TPA::where('date', '>', $tomorrow)->where('date', '<', $after_tomorrow)->get();

        /** Loop all TPA and queue the email */
        foreach ($tpas as $tpa) {
            $user = $tpa->user;
            $this->mailer->queue('emails.tpa_reminder', ['user' => $tpa->user, 'tpa' => $tpa], function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Lembrete de TPA');
            });
        }
    }
}
